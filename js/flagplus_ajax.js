/*
 * @file
 * JavaScript for hiding/showing AJAX element according to JavaScript mode.
 *
 */

(function($) {

  // Re-enable form elements that are disabled for non-ajax situations.
  Drupal.behaviors.enableFormItemsForAjaxForms = {
    attach: function() {
        
    // If ajax is enabled.
    if (Drupal.ajax) {
      $('.enabled-for-ajax').removeAttr('disabled');
    }

    // Show entity type filter button when AJAX off.
    if (!Drupal.ajax) {
      $('html.js .button-filter-entity-type').show();
    }

    // Show AJAX bybundle submit buttons when AJAX enabled.
    if (Drupal.ajax) {
      $('html.js .button-bybundle-ajax').show();
    }

    // Hide non-AJAX bybundle submit buttons when AJAX enabled.
    if (Drupal.ajax) {
      $('html.js .button-bybundle-nojs').hide();
    }
        
  }
  };

})(jQuery);
