<?php

/**
 * @file
 * For admin forms and their handlers (and supporting functions).
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2478 flagplus.admin.inc @endlink.
 */

use Drupal\flagplus\Common;
use Drupal\flagplus\FlagHelper;
use Drupal\flagplus\banner\BannerHelper;

/**
 * Custom page callback function for admin overview page.
 *
 * @return array
 *   A Drupal page render array.
 */
function flagplus_admin_page() {

  // Page render array.
  $page = array();

  $page[Common::MODULE] = array(
    '#markup' => t('!module administration overview.', array('!module' => Common::MODULE_NAME)),
    '#prefix' => '<p><em>',
    '#suffix' => '</em></p>',
  );

  $page['welcome'] = array(
    '#markup' => t('For specific Flag Plus capabilities please visit the other admin tabs or these links:'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  $key_links = 'links';
  $page[$key_links ] = array(
    '#type' => 'container',
    '#attributes' => array(),
  );

  $links = array();
  $links[] = BannerHelper::getLinkAdmin();

  $links[] = FlagHelper::getLinkAdminBybundleEdit();
  $links[] = FlagHelper::getLinkAdminBybundleAjax();

  $linklist = theme('item_list', array('items' => $links));

  $page[$key_links]['internal'] = array(
    '#markup' => $linklist,
  );

  $links_home = array();

  $link_webel = l(
      t('The Flag Plus demonstration site with graphical UML diagrams (external)'),
      'http://drupal7demo.webel.com.au/module/flagplus' // @todo DRY!
      );
  $links_home[] = $link_webel ;

  $link_drupal = l(
      t('The Flag Plus project home at Drupal.org (external)'),
      'https://www.drupal.org/project/flagplus' // @todo DRY!
      );
  $links_home[] = $link_drupal ;

  $linklist_home = theme('item_list', array('items' => $links_home));

  $page[$key_links ]['external']['header'] = array(
    '#markup' => t('For a detailed description of the Flag Plus module please visit') . ':' ,
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  $page[$key_links ]['external']['links'] = array(
    '#markup' => $linklist_home,
  );

  /*
  $page[$key_links ]['external'] = array(
    '#markup' => t('For a description of the Flag Plus module with graphical UML diagrams please visit') . ' ' . $link_webel,
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
   */

  return $page;
}
