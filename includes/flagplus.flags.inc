<?php

/**
 * @file
 * For admin forms and their handlers for flag manipulation.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2482 flagplus.flags.inc @endlink.
 */

use Drupal\flagplus\bybundle\BundleToFlagFormBuilder;
use Drupal\flagplus\bybundle\BybundleAjaxBuilder;
use Drupal\flagplus\bybundle\BybundleFormBuilder;
use Drupal\flagplus\bybundle\BybundlePageBuilder;
use Drupal\flagplus\Common;

module_load_include('inc', Common::MODULE, 'includes/' . Common::MODULE . '.entitytype');

/**
 * Administration page, readonly view of flags by content type bundle.
 *
 * Display tables of all entity type bundles and their applicable flags.
 *
 * @return array
 *   Data array for page theming.
 */
function flagplus_page_bybundle() {
  return BybundlePageBuilder::data(Common::MODULE . '_page_bybundle');
}

/**
 * Theme the output for the flags-by-bundle administration view page.
 *
 * @param array $vars
 *   Variables array as passed to the theming function.
 *
 * @return array
 *   Drupal render array for themed page.
 */
function theme_flagplus_page_bybundle(array $vars) {
  return BybundlePageBuilder::theme($vars);
}

/**
 * Administration page, editable view of flags by content type bundle.
 *
 * Display tables of all entity type bundles and their applicable flags.
 */
function flagplus_page_bybundle_with_subforms() {
  return BybundleFormBuilder::data(Common::MODULE . '_page_bybundle_with_subforms');
}

/**
 * Administration page with embedded editable forms of flags by bundle.
 *
 * Display table of forms of all entity types bundles,
 * and their applicable flags.
 *
 * @param array $vars
 *   Array of vars passed by form handler.
 */
function theme_flagplus_page_bybundle_with_subforms(array $vars) {
  return BybundleFormBuilder::theme($vars);
}

/**
 * Builds a mini-form for applicability of all flags to a given entity bundle.
 *
 * @param array $form
 *   A Form API form array.
 * @param array $form_state
 *   A Form API form state array.
 * @param string $bundle_name
 *   Name of the entity bundle.
 * @param array $flags
 *   Array of flags known to be applicable to this bundle.
 * @param array $noflags
 *   Array of candidate flags known to be NOT applicable to this bundle.
 * @param bool $inside_ajax
 *   If TRUE will add an AJAX submit button with specific AJAX callback,
 *   as well as support for a per-bundle AJAX form partial update zone.
 *   The AJAX button will however only be visible in JavaScript support mode.
 *   (Otherwise a non-AJAX fallback button will be visible).
 *
 * @return array
 *   A Form API form.
 */
function flagplus_form_bundle_to_flag(array $form, array &$form_state, $entity_type, $bundle_name, array $flags, array $noflags, $inside_ajax = FALSE) {
  return BundleToFlagFormBuilder::build($form, $form_state, $entity_type, $bundle_name, $flags, $noflags, $inside_ajax, Common::MODULE . '_form_bundle_to_flag_ajax_callback');
}

/**
 * AJAX callback for the bundle to flag form.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 *
 * @return array
 *   The portion of the form for the flag applicability of the bundle.
 */
function flagplus_form_bundle_to_flag_ajax_callback(array $form, array &$form_state) {
  return BundleToFlagFormBuilder::ajaxCallback($form, $form_state);
}

/**
 * Handles extraction of answer to whether a given flag checked for a bundle.
 *
 * @param array $form
 *   Form API form array.
 * @param array $form_state
 *   Form API form state array.
 */
function flagplus_form_bundle_to_flag_submit(array $form, array $form_state) {
  BundleToFlagFormBuilder::submit($form, $form_state);
}

/**
 * EXPERIMENTAL: AJAX version of a form for applicability of flags by bundle.
 *
 * Has an AJAX entity-type filter select (with fallback to
 * show an additional filter submit button for non-JavaScript).
 *
 * Has one or more embedded sub-forms for choosing applicability of flags
 * by entity bundle (shown according to the entity-type filter choice).
 *
 * @todo BUG: Non-JavaScript fallback only works when the entire parent
 * form with all entity-types bundle sub-forms are shown. If you
 * select an entity type and filter on it, the flag applicability
 * selections are no longer processed.
 *
 * @param array $form
 *   Form API form.
 * @param array $form_state
 *   Form API form state.
 *
 * @return array
 *   Form API form.
 */
function flagplus_form_bybundle_ajax(array $form, array &$form_state) {
  return BybundleAjaxBuilder::build($form, $form_state);
}

/**
 * Only reached if JavaScript disabled.
 *
 * Ensures form state held before calling original form builder again
 * (so non-JavaScript form can access chosen entity type filter state).
 *
 * @param array $form
 *   Form API form array.
 * @param array $form_state
 *   Form API form state array.
 */
function flagplus_form_bybundle_ajax_submit(array $form, array &$form_state) {
  return BybundleAjaxBuilder::submit($form, $form_state);
}
