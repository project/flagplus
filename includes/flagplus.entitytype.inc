<?php

/**
 * @file
 * Administration: form filter for entity type.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2483 flagplus.entitytype.inc @endlink.
 */

use Drupal\flagplus\EntityFilter;
use Drupal\flagplus\bybundle\BybundleAbstractBuilder;

/**
 * Builds a 'select' for the entity-type filter (non-AJAX version).
 *
 * Uses a session variable to obtain the default entity-type filter value.
 *
 * @param array $values
 *   For state values to extract current user-selected entity type from.
 *
 * @return array
 *   A form array for filtering on the entity type.
 */
function flagplus_form_filter_entitytype(array $values) {

  $form['entity-type-select'] = EntityFilter::buildEntityTypeSelect($values);

  return $form;
}

/**
 * Submit handler for the entity-type filter (non-AJAX version).
 *
 * Uses a session variable for holding the entity-type filter value.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 */
function flagplus_form_filter_entitytype_submit(array $form, array &$form_state) {
  EntityFilter::setEntityTypeFromValues($form_state['values']);
}

/**
 * Callback for AJAX entity type filter to refresh entity to flag form portion.
 *
 * @param array $form
 *   Form API form array.
 * @param array $form_state
 *   Form API form state array.
 *
 * @return array
 *   Portion of form with entity type filter (for rebuilding).
 */
function flagplus_form_filter_entitytype_ajax_callback(array $form, array $form_state) {
  return $form[BybundleAbstractBuilder::THEME_VAR_ENTITY2FLAG];
}
