<?php

/**
 * @file
 * For admin forms and their handlers for Flag Plus banner manipulation.
 */

use Drupal\flagplus\Common;
use Drupal\flagplus\banner\BannerSchema;
use Drupal\flagplus\banner\BannerHelper;
use Drupal\flagplus\banner\BannerFlagHelper;

// @todo Consider migrate to class constants (along with form builder).
define('FLAGPLUS_KEY_FIELDSET_DECORATOR', 'fieldset_decorator');
define('FLAGPLUS_KEY_FIELDSET_BLOCK', 'fieldset_block');
define('FLAGPLUS_KEY_DECORATOR_OPTIONS', 'decorator_options');
define('FLAGPLUS_KEY_BLOCK_OPTIONS', 'block_options');

define('FLAGPLUS_WRAPPER_DECORATOR_SETTINGS', 'wrapper-decorator-settings');
define('FLAGPLUS_WRAPPER_DECORATOR_OPTIONS', 'wrapper-decorator-options');
define('FLAGPLUS_WRAPPER_BLOCK_SETTINGS', 'wrapper-block-settings');
define('FLAGPLUS_WRAPPER_BLOCK_OPTIONS', 'wrapper-block-options');

/**
 * Admin form function for banner configuration.
 *
 * @param array $form
 *   A Form API form array.
 * @param array $form_state
 *   A Form API form state array.
 *
 * @return array
 *   A Form API form.
 */
function flagplus_form_admin_banners(array $form, array &$form_state) {

  $values = isset($form_state['values']) ? $form_state['values'] : NULL;

  $form['info'] = array(
    '#markup' => t('A Flag Plus <b>Banner</b> is a simple Flag state indicator strip with a customisable background color, text color, and border color.'),
    '#prefix' => '<p><em>',
    '#suffix' => '</em></p>',
  );

  $form['info_banner_decorator'] = array(
    '#markup' => t('A Flag Plus <b>Banners Decorator</b> is additional page content with Flag Plus <b>banners</b> that may be shown on full node pages and/or teasers.'),
    '#prefix' => '<p><em>',
    '#suffix' => '</em></p>',
  );

  $form['info_banner_block'] = array(
    '#markup' => t('A Flag Plus <b>Banners Block</b> is a block with Flag Plus <b>banners</b>. It only shows on full node pages (not teasers or most Views).'),
    '#prefix' => '<p><em>',
    '#suffix' => '</em></p>',
  );

  $decorator_enabled = BannerHelper::isShowBannerDecorator();
  $block_enabled = BannerHelper::isShowBannerBlock();

  $decorator_checkbox_selected = isset($values) ? (!empty($values[BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR])) : $decorator_enabled;
  $block_checkbox_selected = isset($values) ? (!empty($values[BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_BLOCK])) : $block_enabled;

  $class_fieldset_decorator = Common::MODULE . '-fieldset ' . Common::MODULE . '-fieldset-decorator';
  $form[FLAGPLUS_KEY_FIELDSET_DECORATOR] = array(
    '#type' => 'fieldset',
    '#title' => t('Banners Decorator'),
    '#attributes' => array('classes' => array($class_fieldset_decorator)),
    '#prefix' => '<div id="' . FLAGPLUS_WRAPPER_DECORATOR_SETTINGS . '">',
    '#suffix' => '</div>',
  );

  $div_css_warn = '<div style="color:orange;">';
  if ($block_checkbox_selected) {
    $form[FLAGPLUS_KEY_FIELDSET_DECORATOR]['warn'] = array(
      '#markup' => t('You must disable the Banners Block option to permit this Banners Decorator option !'),
      '#prefix' => $div_css_warn,
      '#suffix' => '</div>',
    );
  }

  // Checkbox field for whether to show the banners decorator.
  $form[FLAGPLUS_KEY_FIELDSET_DECORATOR][BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the Banners Decorator ?'),
    '#default_value' => !$block_checkbox_selected && $decorator_enabled,
    '#description' => t('If TRUE, the banners decorator will be shown for all flaggable node content.') . ' ' .
    t('You can also choose below whether to show a banner for flagged and/or NOT flagged content per-Flag type.'),
    '#disabled' => $block_checkbox_selected,
    '#ajax' => array(
      'callback' => 'flagplus_form_admin_banners_ajax_decorator_options',
    ),
  );

  $form[FLAGPLUS_KEY_FIELDSET_DECORATOR][FLAGPLUS_KEY_DECORATOR_OPTIONS] = array(
    '#type' => 'fieldset',
    '#title' => t('Banners Decorator options'),
    '#prefix' => '<div id="' . FLAGPLUS_WRAPPER_DECORATOR_OPTIONS . '">',
    '#suffix' => '</div>',
  );

  // Checkbox field for whether to show the banners decorator on full pages.
  $form[FLAGPLUS_KEY_FIELDSET_DECORATOR][FLAGPLUS_KEY_DECORATOR_OPTIONS][BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR_FULL] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the Banners Decorator for full pages ?'),
    '#default_value' => BannerHelper::isShowBannerDecoratorFull(),
    '#description' => t('If TRUE, the Banners Decorator will be shown on full node pages.'),
    '#disabled' => !$decorator_checkbox_selected,
  );

  // Checkbox field for whether to show the banners decorator on teasers.
  $form[FLAGPLUS_KEY_FIELDSET_DECORATOR][FLAGPLUS_KEY_DECORATOR_OPTIONS][BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR_TEASER] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the Banners Decorator for teasers ?'),
    '#default_value' => BannerHelper::isShowBannerDecoratorTeaser(),
    '#description' => t('If TRUE, the Banners Decorator will be shown on node teasers.'),
    '#disabled' => !$decorator_checkbox_selected,
  );

  $class_fieldset_block = Common::MODULE . '-fieldset ' . Common::MODULE . '-fieldset-block';
  $form[FLAGPLUS_KEY_FIELDSET_BLOCK] = array(
    '#type' => 'fieldset',
    '#title' => t('Banners Block'),
    '#attributes' => array('classes' => array($class_fieldset_block)),
    '#prefix' => '<div id="' . FLAGPLUS_WRAPPER_BLOCK_SETTINGS . '">',
    '#suffix' => '</div>',
  );

  if ($decorator_checkbox_selected) {
    $form[FLAGPLUS_KEY_FIELDSET_BLOCK]['warn'] = array(
      '#markup' => t('You must disable the Banners Decorator option to permit this Banners Block option !'),
      '#prefix' => $div_css_warn,
      '#suffix' => '</div>',
    );
  }

  // Checkbox field for whether to show the banners block.
  $form[FLAGPLUS_KEY_FIELDSET_BLOCK][BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_BLOCK] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the Banners Block ?'),
    '#default_value' => $block_enabled && (!$decorator_checkbox_selected),
    '#description' => t('If TRUE, the Banners block will be shown for all flaggable node content.') . ' ' .
    t('You can also choose below whether to show a banner for flagged and/or NOT flagged content per-Flag type.') . ' ' .
    t('You may NOT choose to show both the Banners Block and the Banners Decorator at once !') . ' ' .
    t('Remember to also allocate the Banners Block to a region (usually to Content) under:') . ' ' .
    l(t('Blocks'), '/admin/structure/blocks'),
    '#disabled' => $decorator_checkbox_selected,
    '#ajax' => array(
      'callback' => 'flagplus_form_admin_banners_ajax_block_options',
    ),
  );

  $form[FLAGPLUS_KEY_FIELDSET_BLOCK][FLAGPLUS_KEY_BLOCK_OPTIONS] = array(
    '#type' => 'fieldset',
    '#title' => t('Banners Block options'),
    '#prefix' => '<div id="' . FLAGPLUS_WRAPPER_BLOCK_OPTIONS . '">',
    '#suffix' => '</div>',
  );

  // Checkbox field for whether to show the banners block title (usually false).
  $form[FLAGPLUS_KEY_FIELDSET_BLOCK][FLAGPLUS_KEY_BLOCK_OPTIONS][BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_BLOCK_TITLE] = array(
    '#type' => 'checkbox',
    '#title' => t('Style: Show a Banners Block title [NOT RECOMMENDED] ?'),
    '#default_value' => BannerHelper::isShowBannerBlockTitle(),
    '#description' => t('If TRUE, the Banners Block will be shown with a simple block title.') . ' ' .
    t('The entire block along with the flag banners will also be slightly highlighted.') . ' ' .
    t('Usually this should remain FALSE, as a Banners Block title can be distracting.'),
    '#disabled' => !$block_checkbox_selected,
  );

  $key_fieldset_banners = 'banners_banners';
  $class_fieldset_banners = Common::MODULE . '-fieldset ' . Common::MODULE . '-fieldset-banners';
  $form[$key_fieldset_banners] = array(
    '#type' => 'fieldset',
    '#title' => t('Banner styling (all flags)'),
    '#attributes' => array('classes' => array($class_fieldset_banners)),
  );

  // Checkbox field for whether to always use CAPS for the banner flag title.
  $form[$key_fieldset_banners][BannerHelper::VAR_ADMIN_DO_STYLE_CAPS_TITLE] = array(
    '#type' => 'checkbox',
    '#title' => t('Style: Use CAPS for the banner flag title ?'),
    '#default_value' => BannerHelper::isCssStyleCapsTitle(),
    '#description' => t('If TRUE, and if the flag title is shown, it will be in ALL CAPS.') . ' ' .
    t("(When a banner indicates that content is NOT flagged the word 'NOT' is always in CAPS)."),
  );

  // Checkbox field for whether to center the banner messages.
  $form[$key_fieldset_banners][BannerHelper::VAR_ADMIN_DO_CSS_CENTER_BANNERS] = array(
    '#type' => 'checkbox',
    '#title' => t('Style: Center banner messages ?'),
    '#default_value' => BannerHelper::isCssStyleCenterBanners(),
    '#description' => t('If TRUE, the banner messages will be forced centered.') . ' ' .
    t('If FALSE it will default to the CSS style file specification (left-aligned).') . ' ' .
    t('Centering may look fine when only a single flags is applicable for a content type,') . ' ' .
    t('but when many flags are applicable left-alignment may be preferred (seem cleaner).'),
  );

  $form['warn_no_html'] = array(
    '#markup' => t('IMPORTANT: please do not use any HTML markup in the banner message text below !'),
    '#prefix' => '<p><em>',
    '#suffix' => '</em></p>',
  );

  BannerSchema::dbSyncTables();

  $flags = flag_get_flags();
  // Store flags keyed to fid not name (flagplus uses fid as primary key).
  $flagmap = array();
  foreach ($flags as $name => $flag) {
    $flagmap[$flag->fid] = $flag;
  }

  $header_banners = array(
    'flag' => t('Flag'),
    BannerSchema::FIELD_BANNER_FLAG_TITLE_SHOW => t('Show flag title ?'),
    BannerSchema::FIELD_BANNER_MESSAGE_SHOW => t('Show banner message ?'),
    BannerSchema::FIELD_BANNER_ON_FLAGGED => t('Banner on when flagged ?'),
    BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED => t('Banner message when flagged'),
    BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED => t('Background color when flagged'),
    BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED => t('Text color when flagged'),
    BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED => t('Border color when flagged'),
    BannerSchema::FIELD_BANNER_ON_FLAGGED_NOT => t('Banner on when NOT flagged ?'),
    BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED_NOT => t('Banner message when NOT flagged'),
    BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED_NOT => t('Background color when NOT flagged'),
    BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED_NOT => t('Text color when NOT flagged'),
    BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED_NOT => t('Border color when NOT flagged'),
  );

  // Multiple banner form fields per row.
  $rows = array();
  $rows['#tree'] = TRUE;
  $message_size = 28;

  foreach (BannerFlagHelper::dbFetchBannerSettings() as $record) {
    $fid = $record->fid;
    if (array_key_exists($fid, $flagmap)) {
      $name = $flagmap[$fid]->name;

      $rows[$fid] = array(
        'flag' => array(
          '#markup' => $name,
        ),
        BannerSchema::FIELD_BANNER_FLAG_TITLE_SHOW => array(
          '#type' => 'checkbox',
          '#default_value' => $record->{BannerSchema::FIELD_BANNER_FLAG_TITLE_SHOW},
        ),
        BannerSchema::FIELD_BANNER_MESSAGE_SHOW => array(
          '#type' => 'checkbox',
          '#default_value' => $record->{BannerSchema::FIELD_BANNER_MESSAGE_SHOW},
        ),
        BannerSchema::FIELD_BANNER_ON_FLAGGED => array(
          '#type' => 'checkbox',
          '#default_value' => $record->{BannerSchema::FIELD_BANNER_ON_FLAGGED},
        ),
        BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED => array(
          '#type' => 'textfield',
          '#default_value' => $record->{BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED},
          '#size' => $message_size,
        ),
        BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED => BannerFlagHelper::rgbPicker(
            $record->{BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED}
        ),
        BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED => BannerFlagHelper::rgbPicker(
            $record->{BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED}
        ),
        BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED => BannerFlagHelper::rgbPicker(
            $record->{BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED}
        ),
        BannerSchema::FIELD_BANNER_ON_FLAGGED_NOT => array(
          '#type' => 'checkbox',
          '#default_value' => $record->{BannerSchema::FIELD_BANNER_ON_FLAGGED_NOT},
        ),
        BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED_NOT => array(
          '#type' => 'textfield',
          '#default_value' => $record->{BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED_NOT},
          '#size' => $message_size,
        ),
        BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED_NOT => BannerFlagHelper::rgbPicker(
            $record->{BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED_NOT}
        ),
        BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED_NOT => BannerFlagHelper::rgbPicker(
            $record->{BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED_NOT}
        ),
        BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED_NOT => BannerFlagHelper::rgbPicker(
            $record->{BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED_NOT}
        ),
      );
    }
  }

  $form['table'] = array(
    '#theme' => Common::MODULE . '_admin_form_banners_table',
    '#header' => $header_banners,
    'rows' => $rows,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit banner configuration'),
  );

  return $form;
}

/**
 * A themed table of flag plus banner settings.
 *
 * @param array $vars
 *   Form handler vars.
 *
 * @return array
 *   A themed table of flag plus banner settings.
 */
function theme_flagplus_admin_form_banners_table(array &$vars) {

  $form = $vars['form'];
  $rows = $form['rows'];
  $header = $form['#header'];

  // Setup the structure to be rendered and returned.
  $content = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => array(),
  );

  // Traverse each row.  @see element_chidren().
  foreach (element_children($rows) as $row_index) {
    $row = array();
    // Traverse each column in the row.  @see element_children().
    foreach (element_children($rows[$row_index]) as $col_index) {
      // Render the column form element.
      $row[] = drupal_render($rows[$row_index][$col_index]);
    }
    $content['#rows'][] = $row;
  }

  return drupal_render($content);
}

/**
 * AJAX callback to update the form portion for banners decorator options.
 *
 * Also forces update of the form portion for banners block settings,
 * so that if the decorator mode is chosen the block part will be inactive.
 *
 * @param array $form
 *   Form API form array.
 * @param array $form_state
 *   Form API form state array.
 *
 * @return array
 *   A composite ajax command refreshes both the decorator and block options.
 */
function flagplus_form_admin_banners_ajax_decorator_options(array $form, array &$form_state) {

  return array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_replace('#' . FLAGPLUS_WRAPPER_DECORATOR_OPTIONS, render($form[FLAGPLUS_KEY_FIELDSET_DECORATOR][FLAGPLUS_KEY_DECORATOR_OPTIONS])),
      ajax_command_replace("#" . FLAGPLUS_WRAPPER_BLOCK_SETTINGS, render($form[FLAGPLUS_KEY_FIELDSET_BLOCK])),
    ),
  );
}

/**
 * AJAX callback to update the form portion for banners block options.
 *
 * Also forces update of the form portion for banners decorator settings.
 * so that if the block mode is chosen the decorator part will be inactive.
 *
 * @param array $form
 *   Form API form array.
 * @param array $form_state
 *   Form API form state array.
 */
function flagplus_form_admin_banners_ajax_block_options(array $form, array &$form_state) {
  return array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_replace('#' . FLAGPLUS_WRAPPER_BLOCK_OPTIONS, render($form[FLAGPLUS_KEY_FIELDSET_BLOCK][FLAGPLUS_KEY_BLOCK_OPTIONS])),
      ajax_command_replace("#" . FLAGPLUS_WRAPPER_DECORATOR_SETTINGS, render($form[FLAGPLUS_KEY_FIELDSET_DECORATOR])),
    ),
  );
}

/**
 * Implements validation handling for banner configuration.
 *
 * @todo There is a bug in Drupal7 when trying to highlight just a checkbox,
 * so highlighting surrounding fieldset.
 * Visit also @link https://www.drupal.org/node/222380 @endlink.
 *
 * @param array $form
 *   Form API form array.
 * @param array $form_state
 *   Form API form state array.
 */
function flagplus_form_admin_banners_validate(array $form, array &$form_state) {

  $values = $form_state['values'];
  $show_banner_decorator = $values[BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR];

  $show_banner_block = $values[BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_BLOCK];

  if ($show_banner_decorator && $show_banner_block) {

    $message = t('You may not show both a Banners Decorator and a Banners Block at once, please choose to show one or the other (or neither).');

// @codingStandardsIgnoreStart
// Highlighting of error on checkbox fails.
//
// This one should work according to the rules for the #parents array.
//form_set_error(BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_BLOCK, $message);
// This one corresponds to the #array_parents of the item (but fails).
//form_set_error(KEY_FIELDSET_BLOCK . '][' . BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_BLOCK, $message);
// See also 6-year-long issue https://www.drupal.org/node/222380
// Issue: No error highlighting on form checkbox or radio input types.
// @codingStandardsIgnoreEnd
    // WORKAROUND: This highlights the entire surrounding fieldset.
    form_set_error(FLAGPLUS_KEY_FIELDSET_BLOCK, $message);
  }
}

/**
 * Implements submit handling for the banner(s) configuration.
 *
 * Writes global settings that apply across all Flag Plus banners to DB.
 *
 * Writes Flag Plus style and policy options for each Flag to DB.
 *
 * @param array $form
 *   Form API form array.
 * @param array $form_state
 *   Form API form state array.
 */
function flagplus_form_admin_banners_submit(array $form, array &$form_state) {

  $values = $form_state['values'];
  BannerHelper::dbWriteAllBannerSettings($values);
  BannerFlagHelper::dbWriteBanners($values['rows']);
}
