<?php

/**
 * @file
 * BundleToFlagFormBuilder.
 */

namespace Drupal\flagplus\bybundle;

use Drupal\flagplus\Common;
use Drupal\flagplus\AjaxHelper;
use Drupal\flagplus\FlagHelper;

/**
 * Builder and handlers for bundle-to-flag sub forms.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2499 BundleToFlagFormBuilder @endlink.
 *
 * @author darrenkelly
 */
class BundleToFlagFormBuilder {

  /**
   * Key for hidden form variable, entity type.
   *
   * @var string
   */
  const KEY_HIDDEN_ENTITY_TYPE = 'hidden_entity_type';

  /**
   * Key for hidden form variable, bundle name.
   *
   * @var string
   */
  const KEY_HIDDEN_BUNDLE_NAME = 'hidden_bundle_name';

  /**
   * Key for flag applicability form container.
   *
   * @var string
   */
  const KEY_FLAG_APPLICABILITY = 'flag_applicability';

  /**
   * Builds a mini-form for applicability of all flags to a given entity bundle.
   *
   * @param array $form
   *   A Form API form array.
   * @param array $form_state
   *   A Form API form state array.
   * @param string $entity_type
   *   Name of the entity type.
   * @param string $bundle_name
   *   Name of the entity bundle.
   * @param array $flags
   *   Array of flags known to be applicable to this bundle.
   * @param array $noflags
   *   Array of candidate flags known to be NOT applicable to this bundle.
   * @param bool $inside_ajax
   *   If TRUE will add an AJAX submit button with specific AJAX callback,
   *   as well as support for a per-bundle AJAX form partial update zone.
   *   The AJAX button will however only be visible in JavaScript support mode.
   *   (Otherwise a non-AJAX fallback button will be visible).
   * @param string $ajax_callback
   *   Callback to use if using ajax.
   *
   * @return array
   *   A Form API form.
   */
  static public function build(array $form, array &$form_state, $entity_type, $bundle_name, array $flags, array $noflags, $inside_ajax = FALSE, $ajax_callback = NULL) {

    AjaxHelper::attachAjaxHideShow($form);

    // Record as immutable form values the entity_type and bundle_name so
    // available to submit hander.
    $form[self::KEY_HIDDEN_ENTITY_TYPE] = array(
      '#type' => 'value',
      '#value' => $entity_type,
    );
    $form[self::KEY_HIDDEN_BUNDLE_NAME] = array(
      '#type' => 'value',
      '#value' => $bundle_name,
    );

    $header_flags = array(
      'flag' => t('Flag'),
    );

    $default_value = array();

    $options_flags = array();

    // Set all applicable flags to selected.
    foreach ($flags as $flag) {
      $options_flags[$flag->name] = array(
        'flag' => $flag->name,
      );
      $default_value[$flag->name] = TRUE;
    }
    // Set all non-applicable flags to not-selected.
    foreach ($noflags as $flag) {
      $options_flags[$flag->name] = array(
        'flag' => $flag->name,
      );
      $default_value[$flag->name] = FALSE;
    }

    // Sort options alphabetically by flag key, otherwise always get
    // selected before unselected flags in forms,
    // which can cause confusion after change submission.
    ksort($options_flags);

    $form[self::KEY_FLAG_APPLICABILITY][$entity_type][$bundle_name] = array(
      '#type' => 'tableselect',
      '#header' => $header_flags,
      '#options' => $options_flags,
      '#default_value' => $default_value,
      '#empty' => '<em>' . t('No flags for this bundle.') . '</em>',
    );

    // Provide an explicit submit button.
    $do_provide_submit = !(empty($flags) && empty($noflags));
    if ($do_provide_submit) {

      // Always output this element.
      // If JavaScript is enabled use JS to hide() it.
      // Will fallback to use the non-AJAX submit handler.
      $form['submit_nojs'] = array(
        '#type' => 'submit',
        '#value' => t('Submit for bundle (!bundle) NO JS', array('!bundle' => $bundle_name)),
        '#attributes' => array('class' => array('button-bybundle-nojs')),
      );
    }

    if ($do_provide_submit && $inside_ajax) {

      // Always output this but hide using CSS if JavaScript NOT enabled.
      // The CSS should hide it by default so it only appears using JS show().
      $form['submit_ajax'] = array(
        '#type' => 'submit',
        '#value' => t('Submit for bundle (!bundle)', array('!bundle' => $bundle_name)),
        '#attributes' => array('class' => array('button-bybundle-ajax')),
      );

      $wrapper = 'ajax_' . $entity_type . '_' . $bundle_name;

      if ($do_provide_submit) {
        $form[self::KEY_FLAG_APPLICABILITY][$entity_type][$bundle_name]['#prefix'] = '<div id="' . $wrapper . '">';
        $form[self::KEY_FLAG_APPLICABILITY][$entity_type][$bundle_name]['#suffix'] = '</div>';

        // IMPORTANT: must explicitly include this file in this form here,
        // otherwise local callback functions are not seen be /system/ajax.
        form_load_include($form_state, 'inc', Common::MODULE, 'includes/flagplus.flags');

        $form['submit_ajax']['#ajax'] = array(
          'callback' => $ajax_callback,
          'wrapper' => $wrapper,
        );
      }
    }

    return $form;
  }

  /**
   * AJAX callback for the bundle to flag form.
   *
   * @param array $form
   *   Form array.
   * @param array $form_state
   *   Form state array.
   *
   * @return array
   *   The portion of the form for the flag applicability of the bundle.
   */
  static public function ajaxCallback(array $form, array &$form_state) {

    $entity_type = $form_state['values'][self::KEY_HIDDEN_ENTITY_TYPE];
    $bundle_name = $form_state['values'][self::KEY_HIDDEN_BUNDLE_NAME];

    $el = $form[self::KEY_FLAG_APPLICABILITY][$entity_type][$bundle_name];
    return $el;
  }

  /**
   * Submit handler delegate for updating the flag applicability by bundle.
   *
   * @param array $form
   *   Form array.
   * @param array $form_state
   *   Form state array.
   */
  static public function submit(array $form, array $form_state) {

    $values = $form_state['values'];
    $entity_type = $values[self::KEY_HIDDEN_ENTITY_TYPE];
    $bundle_name = $values[self::KEY_HIDDEN_BUNDLE_NAME];

    $flag_check_answers = $values[$bundle_name];

    $flags_checked = array();
    foreach ($flag_check_answers as $key => $flag_answer) {
      $flags_checked[$key] = !empty($flag_answer);
    }
    // Sort flags alphabetically, otherwise always get
    // selected before unselected flags in forms, which
    // can cause confusion after change submission.
    ksort($flags_checked);

    FlagHelper::dbUpdateFlagApplicability($entity_type, $bundle_name, $flags_checked);
  }

}
