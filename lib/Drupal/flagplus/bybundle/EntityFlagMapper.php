<?php

/**
 * @file
 * EntityFlagMapper.
 */

namespace Drupal\flagplus\bybundle;

/**
 * Manages information on flag applicability by entity type and bundle.
 *
 * @author darrenkelly
 */
class EntityFlagMapper {

  /**
   * Array key for bundles grouping.
   *
   * @var string
   */
  const KEY_BUNDLES = 'bundles';


  /**
   * Array key for bundle label.
   *
   * @var string
   */
  const KEY_LABEL = 'label';

  /**
   * Array key for applicable flags.
   *
   * @var string
   */
  const KEY_FLAGS = 'flags';

  /**
   * Array key for non-applicable flags.
   *
   * @var string
   */
  const KEY_NOFLAGS = 'noflags';

  /**
   * A map of entities to applicable flags.
   *
   * @return array
   *   A map of entities to applicable flags.
   */
  static protected function entity2flag() {

    $flags = flag_get_flags();
    $default_flags = flag_get_default_flags(TRUE);

    // @codingStandardsIgnoreStart
    //$flagplus_admin_listing = drupal_get_form('flagplus_admin_listing', $flags);
    // @codingStandardsIgnoreEnd

    $entity_info = entity_get_info();

    $entity2flag = array();

    // Populate mapping array for every bundle first so have available
    // for overview tables, even if a bundle has no flags.
    foreach ($entity_info as $key => $entity_type) {
      $entity2flag[$key] = array();
      $entity2flag[$key][self::KEY_BUNDLES] = array();
      foreach ($entity_type[self::KEY_BUNDLES] as $bundle_name => $bundle) {
        $entity2flag[$key][self::KEY_BUNDLES][$bundle_name][self::KEY_LABEL] = $bundle[self::KEY_LABEL];
        $entity2flag[$key][self::KEY_BUNDLES][$bundle_name][self::KEY_FLAGS] = array();
        $entity2flag[$key][self::KEY_BUNDLES][$bundle_name][self::KEY_NOFLAGS] = array();
      }
    }

    foreach ($entity_info as $key => $entity_type) {
      foreach ($flags as $flag) {
        if ($flag->entity_type == $key) {
          foreach ($entity_type[self::KEY_BUNDLES] as $bundle_name => $bundle) {
            if (in_array($bundle_name, $flag->types)) {
              $entity2flag[$key][self::KEY_BUNDLES][$bundle_name][self::KEY_FLAGS][] = $flag;
            }
            else {
              $entity2flag[$key][self::KEY_BUNDLES][$bundle_name][self::KEY_NOFLAGS][] = $flag;
            }
          }
        }
      }
    }

    return $entity2flag;
  }

  /**
   * Array of flags applicable to the bundle within the entity type.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle_name
   *   The bundle name.
   *
   * @return array
   *   Array of flags applicable to the bundle within the entity type.
   */
  static public function getFlagsApplicable($entity_type, $bundle_name) {
    $entity2flag = self::entity2flag();
    if (empty($entity2flag[$entity_type]) || empty($entity2flag[$entity_type][self::KEY_BUNDLES][$bundle_name])) {
      // @todo warn/throw ?
      return array();
    }
    return $entity2flag[$entity_type][self::KEY_BUNDLES][$bundle_name][self::KEY_FLAGS];
  }

  /**
   * Array of flags not applicable to the bundle within the entity type.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle_name
   *   The bundle name.
   *
   * @return array
   *   Array of flags not applicable to the bundle within the entity type.
   */
  static public function getFlagsNotApplicable($entity_type, $bundle_name) {
    $entity2flag = self::entity2flag();
    if (empty($entity2flag[$entity_type]) || empty($entity2flag[$entity_type][self::KEY_BUNDLES][$bundle_name])) {
      // @todo warn/throw ?
      return array();
    }
    return $entity2flag[$entity_type][self::KEY_BUNDLES][$bundle_name][self::KEY_NOFLAGS];
  }

  /**
   * Label of the bundle within the entity type.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle_name
   *   The bundle name.
   *
   * @return array
   *   Label of the bundle within the entity type.
   */
  static public function getBundleLabel($entity_type, $bundle_name) {
    $entity2flag = self::entity2flag();
    if (empty($entity2flag[$entity_type]) || empty($entity2flag[$entity_type][self::KEY_BUNDLES][$bundle_name])) {
      // @todo warn/throw ?
      return array();
    }
    return $entity2flag[$entity_type][self::KEY_BUNDLES][$bundle_name][self::KEY_LABEL];
  }

  /**
   * An array of string names of all entity types.
   *
   * @return array
   *   An array of string names of all entity types.
   */
  static public function getEntityTypes() {
    $types = array();
    foreach (entity_get_info() as $key => $entity_type) {
      $types[] = $key;
    }
    return $types;
  }

  /**
   * An array of string bundles names within the given entity type.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return array
   *   An array of string bundles names within the given entity type.
   */
  static public function getBundleNamesByEntityType($entity_type) {
    $bundles = array();

    $entity2flag = self::entity2flag();
    if (empty($entity2flag[$entity_type]) || empty($entity2flag[$entity_type][self::KEY_BUNDLES])) {
      // @todo warn/throw ?
      return array();
    }

    foreach ($entity2flag[$entity_type][self::KEY_BUNDLES] as $key => $bundle) {
      $bundles[] = $key;
    }
    return $bundles;
  }

}
