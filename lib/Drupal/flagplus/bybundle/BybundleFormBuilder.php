<?php

/**
 * @file
 * BybundleFormBuilder.
 */

namespace Drupal\flagplus\bybundle;

use Drupal\flagplus\EntityFilter;
use Drupal\flagplus\bybundle\EntityFlagMapper;

/**
 * For building editable pages of flag applicability by bundle.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2495 BybundleFormBuilder @endlink.
 */
class BybundleFormBuilder extends BybundleAbstractBuilder {

  /**
   * Themes the given page variables.
   *
   * Creates a page with an info header,
   * an entity type filter sub-form,
   * and a table of tables of flag applicability
   * by entity type and bundle name as sub-forms.
   *
   * @param array $vars
   *   Page variables to theme.
   *
   * @return array
   *   Themed render array.
   */
  static public function theme(array $vars) {

    $info = $vars[self::THEME_VAR_INFO];

    $page[self::THEME_VAR_INFO] = array(
      '#markup' => $info,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    );

    $page[EntityFilter::keyFilter()] = EntityFilter::buildEntityTypeSelectContainer();

    $page[self::THEME_VAR_ENTITY2FLAG] = self::buildTableEntityToFlagEdit();

    return drupal_render($page);
  }

  /**
   * Builds a render array for forms of flag applicability by entity type.
   *
   * Build tables of tables of sub-forms by entity type and bundle name,
   * filtered by an entity type filter (with an option for all types).
   * Each sub-form enables setting of flag applicability for one bundle.
   *
   * @return array
   *   Render array for a table.
   */
  static protected function buildTableEntityToFlagEdit() {

    $out = array(
      '#type' => 'container',
      '#attributes' => array()
    );

    $header = array(
      t('Entity type / Bundle'),
      t('Flag applicability'),
    );

    foreach (EntityFlagMapper::getEntityTypes() as $entity_type) {

      if (EntityFilter::isPassesEntityTypeFilter($entity_type)) {

        $rows = array();

        // Each bundle gets a separate flag applicability checkboxes table form.
        foreach (EntityFlagMapper::getBundleNamesByEntityType($entity_type) as $bundle_name) {

          $flag_form_id = self::getBundleToFlagFormId($entity_type, $bundle_name);
          $flags = EntityFlagMapper::getFlagsApplicable($entity_type, $bundle_name);
          $noflags = EntityFlagMapper::getFlagsNotApplicable($entity_type, $bundle_name);
          $flag_form = drupal_get_form(
              $flag_form_id, $entity_type, $bundle_name, $flags, $noflags
          );

          $rows[] = array(
            $entity_type . '/' . $bundle_name,
            drupal_render($flag_form),
          );
        }

        $table = theme('table', array('header' => $header, 'rows' => $rows));

        $out[$entity_type] = array('#markup' => $table);
      }
    }
    return $out;
  }

}
