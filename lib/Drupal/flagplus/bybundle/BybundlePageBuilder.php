<?php

/**
 * @file
 * BybundlePageBuilder.
 */

namespace Drupal\flagplus\bybundle;

use Drupal\flagplus\bybundle\EntityFlagMapper;

/**
 * For building readonly pages of flag applicability by bundle.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2496 BybundlePageBuilder @endlink.
 */
class BybundlePageBuilder extends BybundleAbstractBuilder {

  /**
   * Themes the given page variables.
   *
   * Creates a page with and info header
   * and a readonly table of sub-tables of flag applicability
   * by entity type and bundle name.
   *
   * @param array $vars
   *   Page variables to theme.
   *
   * @return array
   *   Themed render array.
   */
  static public function theme(array $vars) {

    $info = $vars[self::THEME_VAR_INFO];

    $page[self::THEME_VAR_INFO] = array(
      '#markup' => $info,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    );

    $page[self::THEME_VAR_ENTITY2FLAG] = self::buildTableEntityToFlagRead();

    return drupal_render($page);
  }

  /**
   * Builds a render array for a table of entity type and flag applicability.
   *
   * @return array
   *   Render array for a table.
   */
  static protected function buildTableEntityToFlagRead() {

    $out = array(
      '#type' => 'container',
      '#attributes' => array()
    );

    $header = array(
      t('Entity type / Bundle'),
      t('Applicable Flags'),
      t('Not Applicable Flags'),
    );

    foreach (EntityFlagMapper::getEntityTypes() as $entity_type) {

      $rows = array();

      foreach (EntityFlagMapper::getBundleNamesByEntityType($entity_type) as $bundle_name) {

        $flags = EntityFlagMapper::getFlagsApplicable($entity_type, $bundle_name);
        $flag_names = array();
        foreach ($flags as $flag) {
          $flag_names[] = $flag->name;
        }
        $flaglist = theme('item_list', array('items' => $flag_names));

        $noflags = EntityFlagMapper::getFlagsNotApplicable($entity_type, $bundle_name);
        $noflag_names = array();
        foreach ($noflag_names as $flag) {
          $noflag_names[] = $flag->name;
        }
        $noflaglist = theme('item_list', array('items' => $noflag_names));

        $rows[] = array(
          $entity_type . '/' . $bundle_name,
          $flaglist,
          $noflaglist,
        );
      }

      $table = theme('table', array('header' => $header, 'rows' => $rows));

      $out[$entity_type] = array('#markup' => $table);
    }
    return $out;
  }

}
