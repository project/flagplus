<?php

/**
 * @file
 * BybundleAbstractBuilder.
 */

namespace Drupal\flagplus\bybundle;

/**
 * Collects constants and helper methods common to all bybundle flag forms.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2488 BybundleAbstractBuilder @endlink.
 */
abstract class BybundleAbstractBuilder {

  /**
   * Name of info theme variable.
   *
   * @var string
   */
  const THEME_VAR_INFO = 'info';

  /**
   * Name of entity to flag map theme array variable.
   *
   * @var string
   */
  const THEME_VAR_ENTITY2FLAG = 'entity2flag';

  /**
   * Constant prefix for bybundle flag applicability sub-form ids.
   *
   * @var string
   */
  const PREFIX_FORM_ID_HOOK_BYBUNDLE = 'flagplus_form_bybundle_';

  /**
   * Form ID for subforms for flags applicability by bundle.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle_name
   *   Bundle name.
   *
   * @return string
   *   A string form id.
   */
  static protected function getBundleToFlagFormId($entity_type, $bundle_name) {
    return self::PREFIX_FORM_ID_HOOK_BYBUNDLE . $entity_type . '_' . $bundle_name;
  }

  /**
   * Builds page data with the given caller as the function to theme.
   *
   * The page data will contain a translated information string
   * and a map of entity type to flag applicability by bundle.
   *
   * @param string $caller
   *   Name of the calling function to theme.
   *
   * @return array
   *   Page data to theme.
   */
  static public function data($caller) {

    $data['#theme'] = array($caller);

    $data['#' . self::THEME_VAR_INFO] = t('Overview of entity type bundles and their applicable and not-applicable flags.');

    return $data;
  }

}
