<?php

/**
 * @file
 * BybundleAjaxBuilder.
 */

namespace Drupal\flagplus\bybundle;

use Drupal\flagplus\EntityFilter;
use Drupal\flagplus\AjaxHelper;
use Drupal\flagplus\bybundle\EntityFlagMapper;

/**
 * EXPERIMENTAL: AJAX version of a form for applicability of flags by bundle.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2497 BybundleAjaxBuilder @endlink.
 */
class BybundleAjaxBuilder extends BybundleAbstractBuilder {

  /**
   * Builds a nested form for applicability of flags by entity type and bundle.
   *
   * Has an AJAX entity-type filter select (with fallback to
   * show an additional filter submit button for non-JavaScript).
   *
   * Has one or more embedded sub-forms for choosing applicability of flags
   * by entity bundle (shown according to the entity-type filter choice).
   *
   * @todo BUG: Non-JavaScript fallback only works when the entire parent
   * form with all entity-types bundle sub-forms are shown. If you
   * select an entity type and filter on it, the flag applicability
   * selections are no longer processed.
   *
   * @param array $form
   *   Form API form.
   * @param array $form_state
   *   Form API form state.
   *
   * @return array
   *   Form API form.
   */
  static public function build(array $form, array &$form_state) {

    $values = isset($form_state['values']) ? $form_state['values'] : NULL;

    AjaxHelper::attachAjaxHideShow($form);

    $info = t('Overview of entity type bundles and their applicable and not-applicable flags.');

    $form[self::THEME_VAR_INFO] = array(
      '#markup' => $info,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    );

    $form['entity-type-select-ajax'] = EntityFilter::buildEntityTypeSelectAjax($values);

    $form[self::THEME_VAR_ENTITY2FLAG] = self::buildTableEntityToFlagEditAjax($values);

    return $form;
  }

  /**
   * Only reached if JavaScript disabled.
   *
   * Ensures form state held before calling original form builder again
   * (so non-JavaScript form can access chosen entity type filter state).
   *
   * @param array $form
   *   Form API form array.
   * @param array $form_state
   *   Form API form state array.
   */
  static public function submit(array $form, array &$form_state) {
    $form_state['rebuild'] = TRUE;
  }

  /**
   * Builds a render array for forms of flag applicability by entity type.
   *
   * Build tables of tables of sub-forms by entity type and bundle name,
   * filtered by an entity type filter (with an option for all types).
   *
   * Each sub-form enables setting of flag applicability for one bundle
   * using a dedicated AJAX submit button.
   *
   * @param array $values
   *   And array of form state values.
   * @param string $wrapper
   *   AJAX form element wrapper id.
   *
   * @return array
   *   Render array for a table.
   */
  static protected function buildTableEntityToFlagEditAjax(array $values = NULL, $wrapper = EntityFilter::AJAX_WRAPPER_ENTITYTYPE) {

    $out = array(
      '#type' => 'container',
      '#prefix' => '<div id=' . $wrapper . '>',
      '#suffix' => '</div>',
      '#attributes' => array()
    );

    $header = array(
      t('Entity type / Bundle'),
      t('Flag applicability'),
    );

    // Build the bybundle nested forms (according to chosen entity-type filter).
    foreach (EntityFlagMapper::getEntityTypes() as $entity_type) {

      $passes = EntityFilter::isPassesEntityTypeFilterFromFormValues($entity_type, $values);
      if ($passes) {

        $rows = array();

        // Each bundle gets a separate flag applicability checkboxes table form.
        foreach (EntityFlagMapper::getBundleNamesByEntityType($entity_type) as $bundle_name) {

          $flag_form_id = self::getBundleToFlagFormId($entity_type, $bundle_name);
          $inside_ajax = TRUE;
          $flags = EntityFlagMapper::getFlagsApplicable($entity_type, $bundle_name);
          $noflags = EntityFlagMapper::getFlagsNotApplicable($entity_type, $bundle_name);

          $flag_form = drupal_get_form(
              $flag_form_id, $entity_type, $bundle_name, $flags, $noflags, $inside_ajax
          );

          $rows[] = array(
            $entity_type . '/' . $bundle_name,
            drupal_render($flag_form),
          );
        }

        $table = theme('table', array('header' => $header, 'rows' => $rows));

        $out[$entity_type] = array('#markup' => $table);
      }
    }
    return $out;
  }

}
