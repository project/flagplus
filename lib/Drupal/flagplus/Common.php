<?php

/**
 * @file
 * Constants common to the entire module.
 */

namespace Drupal\flagplus;

use Drupal\flagplus\IFlagPlusFactory;
use Drupal\flagplus\FlagPlusFactory;

/**
 * Constants common to the entire module.
 *
 * Promotes robust Don't Repeat Yourself (DRY) code rather
 * than error-prone Write Everything Twice (WET) code.
 *
 * Any constant that is used in more than one file SHOULD be defined here
 * (or should be similarly defined in another shared class) !
 *
 * This makes it easier to adapt the entire module code to other modules.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2468 Common @endlink.
 */
class Common {

  /**
   * The module machine name.
   *
   * @var string
   */
  const MODULE = 'flagplus';

  /**
   * The module display name.
   *
   * @var string
   */
  const MODULE_NAME = 'Flag Plus';

  /**
   * The module page path base stem.
   *
   * @var string
   */
  const STEM = 'flagplus';

  /**
   * The module admin configuration page base path.
   *
   * @var string
   */
  const PATH_ADMIN = 'admin/structure/flagplus';

  /**
   * Flags admin access permission.
   *
   * @var string
   */
  const ACCESS_FLAG_ADMIN = 'administer flags';

  /**
   * Banner content access permission.
   *
   * @var string
   */
  const ACCESS_VIEW_BANNERS_CONTENT = 'access flagplus content';

  /**
   * Flags admin access permission.
   *
   * @var string
   */
  const ACCESS_ADMIN_BANNERS = 'administer flagplus banners';

  /**
   * Name of flags theme array variable.
   *
   * @var string
   */
  const THEME_VAR_FLAGS = 'flags';

  /**
   * Name of default flags theme array variable.
   *
   * @var string
   */
  const THEME_VAR_DEFAULT_FLAGS = 'default_flags';

  /**
   * Name of default flags theme array variable.
   *
   * @var string
   */
  const THEME_VAR_ADMIN_LISTING = 'flagplus_admin_listing';

  const NODE_DISPLAY_MODE_FULL = 'full';

  const NODE_DISPLAY_MODE_TEASER = 'teaser';

  /**
   * A factory for the module.
   *
   * @var IFlagPlusFactory
   */
  static private $FACTORY;


  /**
   * Lazily creates a default factory for the module.
   *
   * @return IFlagPlusFactory
   *   A default factory for the module.
   */
  static public function factory() {
    if (empty(self::$FACTORY)) {
      self::$FACTORY = new FlagPlusFactory();
    }
    return self::$FACTORY;
  }

}
