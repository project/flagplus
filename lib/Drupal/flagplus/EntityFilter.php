<?php

/**
 * @file
 * EntityFilter.
 */

namespace Drupal\flagplus;

use Drupal\flagplus\Common;

/**
 * Helper class for filtering by entity type and bundles.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2484 EntityFilter @endlink.
 */
class EntityFilter {

  /**
   * Name of AJAX variable for entity type filter choice.
   *
   * @var string
   */
  const AJAX_VAR_FILTER_ENTITYTYPE = 'flagplus_filter_entitytype_ajax';

  /**
   * The wrapper id for the ajax callback for entity type filter changes.
   *
   * @var string
   */
  const AJAX_WRAPPER_ENTITYTYPE = 'filter-entitytype-ajax-wrapper';

  /**
   * Name of session variable for entity type filter choice.
   *
   * @var string
   */
  const SESSION_VAR_FILTER_ENTITYTYPE = 'flagplus_filter_entitytype';

  /**
   * State indicator for selection '<All>' for entity type filter choice.
   *
   * @var string
   */
  const STATE_VAR_FILTER_ENTITYTYPE_ALL = '0';

  /**
   * CSS class for entity type filter container.
   *
   * @var string
   */
  const CSS_CLASS_FILTER_ENTITYTYPE = 'flagplus-filter-entitytype';

  /**
   * CSS element style for entity type filter container.
   *
   * @var string
   */
  const CSS_STYLE_FILTER_ENTITYTYPE = 'background: #ddd; border: 1px solid silver; width: 30em; padding: 0.0ex 1ex; margin: 0.5ex;';

  /**
   * Form key for the session variable for the entity type filter.
   *
   * @return string
   *   A form key for an entity type filter
   */
  static public function keyFilter() {
    return self::SESSION_VAR_FILTER_ENTITYTYPE;
  }

  /**
   * Sets the entity type from a form state values array.
   *
   * @param array $values
   *   A form state values array.
   */
  static public function setEntityTypeFromValues(array $values) {
    if (!empty($values) && isset($values[self::SESSION_VAR_FILTER_ENTITYTYPE])) {
      $_SESSION[self::SESSION_VAR_FILTER_ENTITYTYPE] = $values[self::SESSION_VAR_FILTER_ENTITYTYPE];
    }
  }

  /**
   * A form container for an enity type select form element.
   *
   * @return array
   *   A form container for an enity type select form element.
   */
  static public function buildEntityTypeSelectContainer() {
    $out = array(
      '#type' => 'container',
      '#attributes' => array(),
    );

    $out['form'] = drupal_get_form('flagplus_form_filter_entitytype');

    return $out;
  }

  /**
   * The name of an entity type, or the ALL placeholder fiter value.
   *
   * @return string
   *   The name of an entity type, or the ALL placeholder fiter value.
   */
  static public function getEntityTypeFilter() {
    if (!isset($_SESSION[self::SESSION_VAR_FILTER_ENTITYTYPE])) {
      $_SESSION[self::SESSION_VAR_FILTER_ENTITYTYPE] = self::STATE_VAR_FILTER_ENTITYTYPE_ALL;
    }
    return $_SESSION[self::SESSION_VAR_FILTER_ENTITYTYPE];
  }

  /**
   * Whether the given entity type passes the entity type filter.
   *
   * @param string $entityType
   *   The name of the entity type.
   *
   * @return bool
   *   Whether the given entity type passes the entity type filter.
   */
  static public function isPassesEntityTypeFilter($entityType) {
    $filter_entitytype = EntityFilter::getEntityTypeFilter();
    return
        $filter_entitytype === self::STATE_VAR_FILTER_ENTITYTYPE_ALL ||
        $filter_entitytype === $entityType;
  }

  /**
   * Extracts the entity type filter value setting from form state values.
   *
   * @param array $values
   *   The values of a form state array.
   *
   * @return string
   *   The name of an entity type to filter on, or the ALL placeholder.
   */
  static public function getEntityTypeFilterFromFormValues(array $values = NULL) {
    return !empty($values[self::AJAX_VAR_FILTER_ENTITYTYPE]) ?
        $values[self::AJAX_VAR_FILTER_ENTITYTYPE] : self::STATE_VAR_FILTER_ENTITYTYPE_ALL;
  }

  /**
   * Whether the given entity type passes the filter specified in the values.
   *
   * @param string $entityType
   *   The name of the entity type.
   * @param array $values
   *   Form state values.
   *
   * @return bool
   *   Whether the given entity type passes the filter specified in
   *   the form state values.
   */
  static public function isPassesEntityTypeFilterFromFormValues($entityType, array $values = NULL) {
    $filter_entitytype = self::getEntityTypeFilterFromFormValues($values);

    return $filter_entitytype === self::STATE_VAR_FILTER_ENTITYTYPE_ALL ||
        $filter_entitytype === $entityType;
  }

  /**
   * Entity type filter options array for a select form element.
   *
   * @return array
   *   Options array for a select form element.
   */
  static protected function buildOptionsEntityTypes() {
    $entity_info = entity_get_info();
    $options = array();
    // To choose all.
    $options['0'] = '<All>';
    foreach ($entity_info as $key => $entity_type) {
      $options[$key] = $key;
    }
    return $options;
  }

  /**
   * Form element for a select with entity type options.
   *
   * @param array $values
   *   Form state values.
   *
   * @return array
   *   Form element for a select with entity type options.
   */
  static public function buildEntityTypeSelect(array $values) {
    $out = array(
      '#type' => 'container',
      '#attributes' => array('class' => array(Common::MODULE . '-wrapper-filter-entitytype')),
    );
    $out[self::SESSION_VAR_FILTER_ENTITYTYPE] = array(
      '#title' => t('Filter by entity type'),
      '#type' => 'select',
      '#description' => 'Choose an entity type to show only those flags possibly applicable to its bundles (custom content types).',
      '#options' => self::buildOptionsEntityTypes(),
    );

    if (isset($_SESSION[self::SESSION_VAR_FILTER_ENTITYTYPE])) {
      $out[self::SESSION_VAR_FILTER_ENTITYTYPE]['#default_value'] = $_SESSION[self::SESSION_VAR_FILTER_ENTITYTYPE];
    }

    $out['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit entity type filter'),
    );

    return $out;
  }

  /**
   * Form element for an AJAX select with entity type options.
   *
   * Has non-JS fallback.
   *
   * @param array $values
   *   Form state values.
   *
   * @return array
   *   Form element for a select with entity type options.
   */
  static public function buildEntityTypeSelectAjax(array $values = NULL) {

    // Get/reset AJAX filter var for entity type.
    $filter_entitytype = self::getEntityTypeFilterFromFormValues($values);

    $out = array(
      '#type' => 'container',
      '#attributes' => array('class' => array(Common::MODULE . '-wrapper-filter-entitytype')),
    );
    $out[self::AJAX_VAR_FILTER_ENTITYTYPE] = array(
      '#title' => t('Filter by entity type'),
      '#type' => 'select',
      '#description' => 'Choose an entity type to show only those flags possibly applicable to its bundles (custom content types).',
      '#options' => self::buildOptionsEntityTypes(),
      '#attributes' => array('class' => array('enabled-for-ajax')),
    );

    $out[self::AJAX_VAR_FILTER_ENTITYTYPE]['#default_value'] = $filter_entitytype;

    $out[self::AJAX_VAR_FILTER_ENTITYTYPE]['#ajax'] = array(
      // @todo DRY: name ?
      'callback' => Common::MODULE . '_form_filter_entitytype_ajax_callback',
      'wrapper' => self::AJAX_WRAPPER_ENTITYTYPE,
    );

    // Always output this element but hide using CSS if JavaScript enabled.
    $out['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit entity type filter'),
      '#attributes' => array('class' => array('button-filter-entity-type')),
    );

    return $out;
  }

}
