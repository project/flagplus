<?php

/**
 * @file
 * IFlagPlusFactory.
 */

namespace Drupal\flagplus;

use Drupal\flagplus\banner\IFlagBanner;

use flag_flag;

/**
 * Simple factory for common products for this module.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2518 IFlagPlusFactory @endlink.
 */
interface IFlagPlusFactory {

  /**
   * A new Flag Plus banner product for a given flag.
   *
   * @param \flag_flag $flag
   *   A flag handler.
   * @param \stdClass $record
   *   (Optional.) A record from a db query compatible with the FlagPlus table.
   *
   * @return IFlagBanner
   *   A new Flag Plus banner product for a given flag.
   */
  public function newFlagBanner(flag_flag $flag, \stdClass $record);

}
