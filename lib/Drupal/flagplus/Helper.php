<?php

/**
 * @file
 * BannerHelper.
 */

namespace Drupal\flagplus;

/**
 * Helper base class with some common static utility methods.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2528 Helper @endlink.
 */
class Helper {

  /**
   * Whether whether to show a message when a variable managed by this is set.
   *
   * @var bool
   */
  private static $showMessages = TRUE;

  /**
   * Sets whether to show a message when a variable managed by this is set.
   *
   * @param bool $showMessages
   *   Whether to show a message when a system variable managed by this is set.
   */
  static public function setShowMessageOnSet($showMessages) {
    self::$showMessages = $showMessages;
  }

  /**
   * Whether to show a message when a system variable managed by this is set.
   *
   * @return bool $showMessages
   *   Whether to show a message when a system variable managed by this is set.
   */
  static protected function isShowMessageOnSet() {
    return self::$showMessages;
  }

  /**
   * For human-friendly output of bool options.
   *
   * @const
   */
  static private $onoff = array(
    FALSE => 'off',
    TRUE => "on",
  );

  /**
   * Writes a drupal message to echo the value of a boolean variable being set.
   *
   * Only actually writes anything if isShowMessageOnSet() otherwise skipped.
   *
   * @param string $var
   *   The (translated) human friendly name of the variable.
   * @param bool $val
   *   The value of a boolean variable.
   */
  static protected function echoSetBool($var, $val) {
    if (self::isShowMessageOnSet()) {
      drupal_set_message(
          t('The value of %variable was set to %value', array('%variable' => $var, '%value' => self::$onoff[$val])));
    }
  }

}
