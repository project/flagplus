<?php

/**
 * @file
 * Helper class for flags.
 */

namespace Drupal\flagplus;

use Drupal\flagplus\Common;

use flag_flag;

/**
 * Helper class for flags.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2501 FlagHelper @endlink.
 *
 * @author darrenkelly
 */
class FlagHelper {

  /**
   * Stem for the admin menu item for viewing flag applicability.
   *
   * @var string
   */
  const STEM_ADMIN_READ = 'bybundle/read';

  /**
   * Gets a link for the flag applicability admin read page.
   *
   * @param string $title
   *   (Optional.) A translated title for the link (overrides a default).
   *
   * @return string
   *   A Drupal link markup.
   */
  static public function getLinkAdminBybundleRead($title = NULL) {
    $title = empty($title) ? t('View flag applicability conveniently by entity type and bundle') : $title;
    return l($title, Common::PATH_ADMIN . '/' . self::STEM_ADMIN_READ);
  }

  /**
   * Stem for the admin menu item for editing flag applicability (non AJAX).
   *
   * @var string
   */
  const STEM_ADMIN_EDIT = 'bybundle/edit';

  /**
   * Gets a link for the flag applicability admin edit page.
   *
   * @param string $title
   *   (Optional.) A translated title for the link (overrides a default).
   *
   * @return string
   *   A Drupal link markup.
   */
  static public function getLinkAdminBybundleEdit($title = NULL) {
    $title = empty($title) ? t('Edit flag applicability conveniently by entity type and bundle') : $title;
    return l($title, Common::PATH_ADMIN . '/' . self::STEM_ADMIN_EDIT);
  }

  /**
   * Stem for the admin menu item for editing flag applicability (AJAX).
   *
   * @var string
   */
  const STEM_ADMIN_AJAX = 'bybundle/ppu';

  /**
   * Gets a link for the flag applicability admin edit AJAX page.
   *
   * @param string $title
   *   (Optional.) A translated title for the link (overrides a default).
   *
   * @return string
   *   A Drupal link markup.
   */
  static public function getLinkAdminBybundleAjax($title = NULL) {
    $title = empty($title) ? t('Edit flag applicability conveniently by entity type and bundle (AJAX mode)') : $title;
    return l($title, Common::PATH_ADMIN . '/' . self::STEM_ADMIN_AJAX);
  }

  /**
   * Gets the menu item path for the bybundle admin edit form.
   *
   * @return string
   *   Menu item path.
   */
  static protected function getMenuPathBybundleEdit() {
    return Common::PATH_ADMIN . '/' . self::STEM_ADMIN_EDIT;
  }

  /**
   * Adds a menu item array for editable form view of flags by entity bundle.
   *
   * @param array $items
   *   Menu items array.
   * @param int $weight
   *   Weight.
   */
  static public function addMenuArrayBybundleEdit(array &$items, $weight = 0) {
    $items[self::getMenuPathBybundleEdit()] = array(
      'weight' => $weight,
      'title' => 'Flags by entity bundle',
      'page callback' => Common::MODULE . '_page_bybundle_with_subforms',
      'access callback' => 'user_access',
      'access arguments' => array(Common::ACCESS_FLAG_ADMIN),
      'file' => 'includes/' . Common::MODULE . '.flags.inc',
      'type' => MENU_LOCAL_TASK,
    );
  }

  /**
   * Gets the menu item path for the bybundle flag admin ajax form.
   *
   * @return string
   *   Menu item path.
   */
  static protected function getMenuPathBybundleAjax() {
    return Common::PATH_ADMIN . '/' . self::STEM_ADMIN_AJAX;
  }

  /**
   * Adds a menu item array for AJAX edit form view of flags by entity bundle.
   *
   * @param array $items
   *   Menu items array.
   * @param int $weight
   *   Weight.
   */
  static public function addMenuArrayBybundleAjax(array &$items, $weight = 0) {
    $items[self::getMenuPathBybundleAjax()] = array(
      'weight' => $weight,
      'title' => 'Flags by entity bundle (AJAX)',
      'page callback' => 'drupal_get_form',
      'page arguments' => array(Common::MODULE . '_form_bybundle_ajax'),
      'access callback' => 'user_access',
      'access arguments' => array(Common::ACCESS_FLAG_ADMIN),
      'file' => 'includes/' . Common::MODULE . '.flags.inc',
      'type' => MENU_LOCAL_TASK,
    );
  }

  /**
   * Gets the menu item path for the bybundle flag admin view (DEBUG) form.
   *
   * @return string
   *   Menu item path.
   */
  static protected function getMenuPathBybundleRead() {
    return Common::PATH_ADMIN . '/' . self::STEM_ADMIN_READ;
  }

  /**
   * Adds a menu item array for the readonly view of flags by entity bundle.
   *
   * @param array $items
   *   Menu items array.
   * @param int $weight
   *   Weight.
   */
  static public function addMenuArrayBybundleRead(array &$items, $weight = 0) {
    $items[self::getMenuPathBybundleRead()] = array(
      'weight' => $weight,
      'title' => 'Flags by entity bundle (readonly)',
      'page callback' => Common::MODULE . '_page_bybundle',
      'access callback' => 'user_access',
      'access arguments' => array(Common::ACCESS_FLAG_ADMIN),
      'file' => 'includes/' . Common::MODULE . '.flags.inc',
      'type' => MENU_LOCAL_TASK,
    );
  }

  /**
   * Whether the given flag is applicable to the given entity type and bundle.
   *
   * @param \flag_flag $flag
   *   The flag handler to check.
   * @param string $entity_type
   *   The entity type to check against.
   * @param string $bundle
   *   The bundle name to check against.
   *
   * @return bool
   *   TRUE if the Flag may be applicable to the given bundle, otherwise FALSE.
   */
  static public function flagMayApply(flag_flag $flag, $entity_type, $bundle) {
    return ($flag->entity_type === $entity_type && in_array($bundle, $flag->types));
  }

  /**
   * Update the applicability settings of a set of selected flags.
   *
   * See also flag_flag.inc for save(), update(), and insert(), which
   * act on the 'flag' and 'flag_types' tables.
   *
   * @param string $entity_type
   *   Name of entity type.
   * @param string $bundle_name
   *   Name of bundle type.
   * @param array $flags_checked
   *   Array of flags that have been checked as applicable.
   */
  static public function dbUpdateFlagApplicability($entity_type, $bundle_name, array $flags_checked) {

    $flags = flag_get_flags();

    foreach ($flags_checked as $flag_name => $flag_checked) {

      $flag = $flags[$flag_name];

      // Only perform further checks if the entity type matches.
      if ($flag->entity_type == $entity_type) {

        if ($flag_checked) {

          // Test whether need add the bundle for this flag.
          if (!in_array($bundle_name, $flag->types)) {
            $flag->types[] = $bundle_name;
            $flag->save();
            drupal_set_message(
                t("Flag '!flag_name' set as applicable to bundle name '!bundle_name' within entity type '!entity_type'", array(
              '!flag_name' => $flag_name,
              '!entity_type' => $entity_type,
              '!bundle_name' => $bundle_name,
                    )
                )
            );
          }
        }
        else {

          // Test whether need remove the bundle for this flag.
          if (in_array($bundle_name, $flag->types)) {
            // Remove $bundle_name and reindex $flag->types
            $flag->types = array_merge(array_diff($flag->types, array($bundle_name)));
            $flag->save();
            drupal_set_message(
                t("Flag '!flag_name' set as NOT applicable to bundle name '!bundle_name' within entity type '!entity_type'", array(
              '!flag_name' => $flag_name,
              '!entity_type' => $entity_type,
              '!bundle_name' => $bundle_name,
                    )
                )
            );
          }
        }
      }
    }
  }

}
