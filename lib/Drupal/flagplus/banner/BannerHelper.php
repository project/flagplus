<?php

/**
 * @file
 * BannerHelper.
 */

namespace Drupal\flagplus\banner;

use Drupal\flagplus\Common;
use Drupal\flagplus\FlagHelper;
use Drupal\flagplus\banner\BannerFlagHelper;
use Drupal\flagplus\Helper;
use stdClass;

/**
 * Helper class for option settings and aspects common to all banners.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2480 BannerHelper @endlink.
 */
class BannerHelper extends Helper {

  /**
   * Stem for the admin menu item for Banners.
   *
   * @var string
   */
  const STEM_ADMIN = 'banners';

  /**
   * Gets a link for the banners admin page.
   *
   * @param string $title
   *   (Optional.) A translated title for the link (overrides a default).
   *
   * @return string
   *   A Drupal link markup.
   */
  static public function getLinkAdmin($title = NULL) {
    $title = empty($title) ? t('Choose options and style parameters for Flag Plus banners per flag') : $title;
    return l($title, Common::PATH_ADMIN . '/' . self::STEM_ADMIN);
  }

  /**
   * Adds a menu item array for the banners admin form.
   *
   * @param array $items
   *   Menu items array.
   * @param int $weight
   *   Weight.
   */
  static public function addMenuArrayAdmin(array &$items, $weight = 0) {
    $items[self::getMenuPathAdmin()] = array(
      'weight' => $weight,
      'title' => Common::MODULE_NAME . ' banners',
      'description' => 'Configure the ' . Common::MODULE_NAME . ' banners',
      'page callback' => 'drupal_get_form',
      'page arguments' => array(Common::MODULE . '_form_admin_banners'),
      'access arguments' => array(Common::ACCESS_ADMIN_BANNERS),
      'type' => MENU_LOCAL_TASK,
      'file' => 'includes/' . Common::MODULE . '.banners.inc',
    );
  }

  /**
   * Gets the menu item path for the banners admin form.
   *
   * @return string
   *   Menu item path.
   */
  static protected function getMenuPathAdmin() {
    return Common::PATH_ADMIN . '/' . self::STEM_ADMIN;
  }

  /**
   * Name of admin configuration variable for whether show banners decorator.
   *
   * @var stringx
   */
  const VAR_ADMIN_DO_SHOW_BANNER_DECORATOR = 'flagplus_var_admin_do_show_banner_decorator';

  /**
   * Whether to show the Banners Decorator at all.
   *
   * @return bool
   *   Whether to show the Banners Decorator at all.
   */
  static public function isShowBannerDecorator() {
    return variable_get(self::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR, TRUE);
  }

  /**
   * Sets whether to show the Banners Decorator at all.
   *
   * POLICY: this should not be on at the same time
   * as the Banners Block !
   *
   * @param bool $showBannerDecorator
   *   Whether to show the Banners Decorator at all.
   */
  static public function setShowBannerDecorator($showBannerDecorator) {
    if ($showBannerDecorator !== self::isShowBannerDecorator()) {
      variable_set(self::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR, $showBannerDecorator);
      self::echoSetBool(t('Banners Decorator display'), $showBannerDecorator);
    }
  }

  /**
   * Name of admin config var for whether show banners decorator on full pages.
   *
   * @var string
   */
  const VAR_ADMIN_DO_SHOW_BANNER_DECORATOR_FULL = 'flagplus_var_admin_do_show_banner_decorator_full';

  /**
   * Whether to show the banners decorator on full node pages.
   *
   * @return bool
   *   Whether to show the banners decorator on full node pages.
   */
  static public function isShowBannerDecoratorFull() {
    return variable_get(self::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR_FULL, TRUE);
  }

  /**
   * Sets whether to show the banners decorator on full node pages.
   *
   * @param bool $showBannerDecoratorFull
   *   Whether to show the banners decorator on full node pages.
   */
  static public function setShowBannerDecoratorFull($showBannerDecoratorFull) {
    if ($showBannerDecoratorFull !== self::isShowBannerDecoratorFull()) {
      variable_set(self::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR_FULL, $showBannerDecoratorFull);
      self::echoSetBool(t('Banners Decorator full page display'), $showBannerDecoratorFull);
    }
  }

  /**
   * Name of admin config var for whether to show banners decorator on teasers.
   *
   * @var string
   */
  const VAR_ADMIN_DO_SHOW_BANNER_DECORATOR_TEASER = 'flagplus_var_admin_do_show_banner_decorator_teaser';

  /**
   * Whether to show the banners decorator on teasers.
   *
   * @return bool
   *   Whether to show the banners decorator on teasers.
   */
  static public function isShowBannerDecoratorTeaser() {
    return variable_get(self::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR_TEASER, TRUE);
  }

  /**
   * Sets whether to show the banners decorator on teasers.
   *
   * @param bool $showBannerDecoratorTeaser
   *   Whether to show the banners decorator on teasers.
   */
  static public function setShowBannerDecoratorTeaser($showBannerDecoratorTeaser) {
    if ($showBannerDecoratorTeaser !== self::isShowBannerDecoratorTeaser()) {
      variable_set(BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR_TEASER, $showBannerDecoratorTeaser);
      self::echoSetBool(t('Banners Decorator teaser display'), $showBannerDecoratorTeaser);
    }
  }

  /**
   * Name of admin config variable for whether to show the banners block.
   *
   * @var string
   */
  const VAR_ADMIN_DO_SHOW_BANNER_BLOCK = 'flagplus_var_admin_do_show_banner_block';

  /**
   * Whether to show the banners block.
   *
   * @return bool
   *   Whether to show the banners block.
   */
  static public function isShowBannerBlock() {
    return variable_get(self::VAR_ADMIN_DO_SHOW_BANNER_BLOCK, FALSE);
  }

  /**
   * Sets whether to show the banners block.
   *
   * @param bool $showBannerBlock
   *   Whether to show the banners block.
   */
  static public function setShowBannerBlock($showBannerBlock) {
    if ($showBannerBlock !== self::isShowBannerBlock()) {
      variable_set(BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_BLOCK, $showBannerBlock);
      self::echoSetBool(t('Banners Block display'), $showBannerBlock);
    }
  }

  /**
   * Name of admin configuration variable for whether show banners block title.
   *
   * @var string
   */
  const VAR_ADMIN_DO_SHOW_BANNER_BLOCK_TITLE = 'flagplus_var_admin_do_show_banner_block_title';

  /**
   * Whether to show a title for the banners block.
   *
   * @return bool
   *   Whether to show a title for the banners block.
   */
  static public function isShowBannerBlockTitle() {
    return variable_get(self::VAR_ADMIN_DO_SHOW_BANNER_BLOCK_TITLE, FALSE);
  }

  /**
   * Sets Whether to show a title for the banners block.
   *
   * @param bool $showBannerBlockTitle
   *   Whether to show a title for the banners block..
   */
  static public function setShowBannerBlockTitle($showBannerBlockTitle) {
    if ($showBannerBlockTitle !== self::isShowBannerBlockTitle()) {
      variable_set(BannerHelper::VAR_ADMIN_DO_SHOW_BANNER_BLOCK_TITLE, $showBannerBlockTitle);
      self::echoSetBool(t('Banners Block title'), $showBannerBlockTitle);
    }
  }

  /**
   * Name of admin configuration variable for whether to center banner messages.
   *
   * @var string
   */
  const VAR_ADMIN_DO_CSS_CENTER_BANNERS = 'flagplus_var_admin_do_css_center_banners';

  /**
   * Whether to center the banners.
   *
   * @return bool
   *   Whether to center the banners.
   */
  static public function isCssStyleCenterBanners() {
    return variable_get(self::VAR_ADMIN_DO_CSS_CENTER_BANNERS, FALSE);
  }

  /**
   * Sets whether to center the banners.
   *
   * @param bool $cssStyleCenterBanners
   *   Whether to center the banners.
   */
  static public function setCssStyleCenterBanners($cssStyleCenterBanners) {
    if ($cssStyleCenterBanners !== self::isCssStyleCenterBanners()) {
      variable_set(BannerHelper::VAR_ADMIN_DO_CSS_CENTER_BANNERS, $cssStyleCenterBanners);
      self::echoSetBool(t('Banners message centering mode set %css_center_banners'), $cssStyleCenterBanners);
    }
  }

  /**
   * Name of admin config variable for whether to capitalise banner titles.
   *
   * @var string
   */
  const VAR_ADMIN_DO_STYLE_CAPS_TITLE = 'flagplus_var_admin_do_style_caps_title';

  /**
   * Whether to use CAPS in the Flag Plus banner labels/titles.
   *
   * @return bool
   *   Whether to use CAPS in the Flag Plus banner labels/titles.
   */
  static public function isCssStyleCapsTitle() {
    return variable_get(self::VAR_ADMIN_DO_STYLE_CAPS_TITLE, TRUE);
  }

  /**
   * Sets whether to use CAPS in the Flag Plus banner labels/titles.
   *
   * @param bool $cssStyleCapsTitle
   *   Whether to use CAPS in the Flag Plus banner labels/titles.
   */
  static public function setCssStyleCapsTitle($cssStyleCapsTitle) {
    if ($cssStyleCapsTitle !== self::isCssStyleCapsTitle()) {
      variable_set(BannerHelper::VAR_ADMIN_DO_STYLE_CAPS_TITLE, $cssStyleCapsTitle);
      self::echoSetBool(t('Banners flag title CAPS mode'), $cssStyleCapsTitle);
    }
  }

  /**
   * Sets all banner options at once from the values of a form state array.
   *
   * Applies only to global banner options, does not affect per-flag options.
   *
   * The options values are ultimately written as per-module system variables.
   *
   * @param array $values
   *   Form state array values.
   */
  static public function dbWriteAllBannerSettings(array $values) {

    $show_banner_decorator = $values[self::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR];
    self::setShowBannerDecorator($show_banner_decorator);

    if ($show_banner_decorator) {
      $show_banner_decorator_full = $values[self::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR_FULL];
      self::setShowBannerDecoratorFull($show_banner_decorator_full);

      $show_banner_decorator_teaser = $values[self::VAR_ADMIN_DO_SHOW_BANNER_DECORATOR_TEASER];
      self::setShowBannerDecoratorTeaser($show_banner_decorator_teaser);
    }

    $show_banner_block = $values[self::VAR_ADMIN_DO_SHOW_BANNER_BLOCK];
    self::setShowBannerBlock($show_banner_block);

    if ($show_banner_block) {
      $show_banner_block_title = $values[self::VAR_ADMIN_DO_SHOW_BANNER_BLOCK_TITLE];
      self::setShowBannerBlockTitle($show_banner_block_title);
    }

    $css_center_banners = $values[self::VAR_ADMIN_DO_CSS_CENTER_BANNERS];
    self::setCssStyleCenterBanners($css_center_banners);

    $style_caps_title = $values[self::VAR_ADMIN_DO_STYLE_CAPS_TITLE];
    self::setCssStyleCapsTitle($style_caps_title);
  }

  const BLOCK_DELTA_BANNER = 'flagplus-banner';

  /**
   * Add the block info for the Banners block into the block info array..
   *
   * @param array $blocks
   *   The block info array.
   */
  static public function addBlockInfo(array &$blocks) {
    $blocks[self::BLOCK_DELTA_BANNER] = array(
      'info' => t('!module', array('!module' => Common::MODULE_NAME)),
      'cache' => DRUPAL_CACHE_PER_ROLE,
    );
  }

  /**
   * Builds a block view array for the flag Banners.
   *
   * @return array
   *   A block view arary (empty if block banners are switched off).
   */
  static public function getBlockView() {
    $block = array();

    if (self::isShowBannerBlock()) {

      // Test whether within a content node.
      $entity_type = 'node';
      if (arg(0) == $entity_type && is_numeric(arg(1))) {
        $node = node_load(arg(1));

        if (user_access(Common::ACCESS_VIEW_BANNERS_CONTENT)) {

          if (self::isShowBannerBlockTitle()) {
            $block['subject'] = t('Flag states');
          }

          $block['classes_array'] = array(Common::MODULE . '-banner-block');

          $block['content']['banners'] = self::buildBanners($node);
        }
      }
    }
    return $block;
  }

  /**
   * Preprocesses the Banners block classes to show a block title (if set).
   *
   * @param array $classes
   *   Classes array for the block to preprocess.
   */
  static public function preprocessBlockClasses(array &$classes) {
    if (self::isShowBannerBlockTitle()) {
      $classes[] = 'block-' . Common::MODULE . '-banner-with-title';
    }
  }

  /**
   * An array of flagplus banners (if any applicable) for a given node.
   *
   * @param object $node
   *   The node to add banners to.
   *
   * @return array
   *   A render array with flagplus banners (if any applicable).
   */
  static public function buildBanners($node) {

    $banners = array();

    $entity_type = 'node';
    $nid = $node->nid;
    $bundle = $node->type;

    // For every flag indicate flagged or not-flagged status.
    $flags = flag_get_flags($entity_type);

    // Store flags keyed to fid not name (flagplus uses fid as primary key).
    $flagmap = array();
    foreach ($flags as $name => $flag) {
      $flagmap[$flag->fid] = $flag;
    }

    // Loop over flagplus table to get banner properties.
    // @todo Use Flag or Flag Plus weighting ?
    foreach (BannerFlagHelper::dbFetchBannerSettings() as $record) {

      $fid = $record->fid;

      if (array_key_exists($fid, $flagmap)) {
        $flag = $flagmap[$fid];
        $name = $flag->name;

        if (FlagHelper::flagMayApply($flag, $entity_type, $bundle)) {

          $flagBanner = Common::factory()->newFlagBanner($flag, $record);
          $flagged = $flag->is_flagged($nid);
          if ($flagged) {
            if ($flagBanner->getDataFlagged()->isShow()) {
              $banners['flagged'][$name] = $flagBanner->buildBanner($flagged);
            }
          }
          else {
            if ($flagBanner->getDataNotFlagged()->isShow()) {
              $banners['flagged_not'][$name] = $flagBanner->buildBanner($flagged);
            }
          }
        }
      }
    }

    return $banners;
  }

  /**
   * If banners decorator mode is on, decorates the node content with banners.
   *
   * @param stdClass $node
   *   The node to decorate.
   * @param string $view_mode
   *   The node display mode.
   */
  static public function addBannersDecoratorToNode(stdClass &$node, $view_mode) {
    $do_show = BannerHelper::isShowBannerDecorator();
    if ($do_show) {
      $do_show_full = BannerHelper::isShowBannerDecoratorFull();
      $do_show_teaser = BannerHelper::isShowBannerDecoratorTeaser();
      if (($view_mode === Common::NODE_DISPLAY_MODE_FULL && $do_show_full) || ($view_mode === Common::NODE_DISPLAY_MODE_TEASER && $do_show_teaser)) {
        $key_banners = Common::MODULE . '_banners';
        $node->content[$key_banners] = BannerHelper::buildBanners($node);
        $node->content[$key_banners]['#weight'] = -999;
      }
    }
  }

}
