<?php

/**
 * @file
 * Helper class for banners.
 */

namespace Drupal\flagplus\banner;

use Drupal\flagplus\banner\BannerSchema;

/**
 * Helper class for aspects of banners specific to a Flag.
 *
 * Includes some simple database interaction helper methods.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2514 BannerFlagHelper @endlink.
 */
class BannerFlagHelper {

  /**
   * JQuery colorpicker render array for monitoring and choosing a color.
   *
   * @todo Refactor ? Perhaps better placed elsewhere.
   *
   * @todo Validation of 6-char HEX $color.
   *
   * @param string $color
   *   Color as hex color string.
   *
   * @return array
   *   JQuery colorpicker form array snippet for monitoring/choosing a color.
   */
  static public function rgbPicker($color) {
    return array(
      '#type' => 'container',
      'input' => array(
        '#type' => 'jquery_colorpicker',
        '#default_value' => $color,
      ),
    );
  }

  /**
   * Fetches the result of a query over all rows of the banner settings.
   *
   * @return DatabaseStatementInterface
   *   A prepared statement object, already executed, suitable for iteration,
   */
  static public function dbFetchBannerSettings() {
    return db_query('SELECT * FROM {' . BannerSchema::TABLE . '}');
  }

  /**
   * Writes a row record for the Flag Plus banner settings for one Flag.
   *
   * @param array $record
   *   Record for row of banner settings keyed to the flag's fid.
   */
  static public function dbWriteBannerSettingsRecord(array $record) {
    // Needed when not a new record.
    $primary_keys = array(BannerSchema::FIELD_FID);
    drupal_write_record(BannerSchema::TABLE, $record, $primary_keys);
  }

  /**
   * Writes to DB the Flag Plus banner settings for every flag.
   *
   * @param array $rows
   *   A set of DB row records for Flag Plus banner settings for every flag.
   */
  static public function dbWriteBanners(array $rows) {
    $input = 'input';
    foreach ($rows as $fid => $row) {
      $record = array(
        BannerSchema::FIELD_FID => $fid,
        BannerSchema::FIELD_BANNER_FLAG_TITLE_SHOW => $row[BannerSchema::FIELD_BANNER_FLAG_TITLE_SHOW],
        BannerSchema::FIELD_BANNER_MESSAGE_SHOW => $row[BannerSchema::FIELD_BANNER_MESSAGE_SHOW],
        BannerSchema::FIELD_BANNER_ON_FLAGGED => $row[BannerSchema::FIELD_BANNER_ON_FLAGGED],
        BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED => $row[BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED],
        BannerSchema::FIELD_BANNER_ON_FLAGGED_NOT => $row[BannerSchema::FIELD_BANNER_ON_FLAGGED_NOT],
        BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED_NOT => $row[BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED_NOT],
        BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED => $row[BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED][$input],
        BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED_NOT => $row[BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED_NOT][$input],
        BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED => $row[BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED][$input],
        BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED_NOT => $row[BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED_NOT][$input],
        BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED => $row[BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED][$input],
        BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED_NOT => $row[BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED_NOT][$input],
      );
      self::dbWriteBannerSettingsRecord($record);
    }
  }

  const CSS_COLOR_DEFAULT_BG_FLAGGED = 'bbffbb';
  const CSS_COLOR_DEFAULT_BG_FLAGGED_NOT = 'ffbbbb';
  const CSS_COLOR_DEFAULT_TEXT_FLAGGED = '333333';
  const CSS_COLOR_DEFAULT_TEXT_FLAGGED_NOT = '333333';
  const CSS_COLOR_DEFAULT_BORDER_FLAGGED = '777777';
  const CSS_COLOR_DEFAULT_BORDER_FLAGGED_NOT = '777777';

}
