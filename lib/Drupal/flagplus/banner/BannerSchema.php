<?php

/**
 * @file
 * Constants common to the database schema of the module.
 */

namespace Drupal\flagplus\banner;

use Drupal\flagplus\banner\BannerFlagHelper;

/**
 * Constants common to the database schema of the module.
 *
 * Promotes robust Don't Repeat Yourself (DRY) code rather
 * than error-prone Write Everything Twice (WET) code.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2500 Schema @endlink.
 */
class BannerSchema {

    const TABLE = 'flagplus_banners';
    const FIELD_FID = 'fid';
    const FIELD_BANNER_ON_FLAGGED = 'banner_on_flagged';
    const FIELD_BANNER_ON_FLAGGED_NOT = 'banner_on_flagged_not';
    const FIELD_BANNER_MESSAGE_FLAGGED = 'banner_message_flagged';
    const FIELD_BANNER_MESSAGE_FLAGGED_NOT = 'banner_message_flagged_not';
    const FIELD_BANNER_FLAG_TITLE_SHOW = 'banner_flag_title_show';
    const FIELD_BANNER_MESSAGE_SHOW = 'banner_message_show';
    const FIELD_BANNER_COLOR_TEXT_FLAGGED = 'banner_color_text_flagged';
    const FIELD_BANNER_COLOR_TEXT_FLAGGED_NOT = 'banner_color_text_flagged_not';
    const FIELD_BANNER_COLOR_BG_FLAGGED = 'banner_color_bg_flagged';
    const FIELD_BANNER_COLOR_BG_FLAGGED_NOT = 'banner_color_bg_flagged_not';
    const FIELD_BANNER_COLOR_BORDER_FLAGGED = 'banner_color_border_flagged';
    const FIELD_BANNER_COLOR_BORDER_FLAGGED_NOT = 'banner_color_border_flagged_not';

    /**
     * Drupal DB schema array for the banners table.
     *
     * @return array
     *   Drupal DB schema array for the banners table.
     */
    static public function getSchema() {
        return array(
          'description' => 'Flag Plus objects for Flag banner tracking.',
          'fields' => array(
            self::FIELD_FID => array(
              'description' => 'The unique Flag ID for the handled flag.',
              // Not serial since always from Flag table (no auto increment here).
              'type' => 'int',
              'size' => 'small',
              'unsigned' => TRUE,
              'not null' => TRUE,
            ),
            self::FIELD_BANNER_FLAG_TITLE_SHOW => array(
              'description' => 'Whether to show the flag title in the banner.',
              'type' => 'int',
              'not null' => TRUE,
              'default' => '1',
            ),
            self::FIELD_BANNER_MESSAGE_SHOW => array(
              'description' => 'Whether to show a custom banner message in the banner.',
              'type' => 'int',
              'not null' => TRUE,
              'default' => '1',
            ),
            self::FIELD_BANNER_ON_FLAGGED => array(
              'description' => 'Whether to show banner messages when flagged.',
              'type' => 'int',
              'not null' => TRUE,
              'default' => '1',
            ),
            self::FIELD_BANNER_MESSAGE_FLAGGED => array(
              'description' => 'A custom banner message for this flag when flagged',
              'type' => 'varchar',
              'length' => '256',
              'not null' => FALSE,
              'default' => '',
            ),
            self::FIELD_BANNER_ON_FLAGGED_NOT => array(
              'description' => 'Whether to show banner messages when NOT flagged.',
              'type' => 'int',
              'not null' => TRUE,
              'default' => '1',
            ),
            self::FIELD_BANNER_MESSAGE_FLAGGED_NOT => array(
              'description' => 'A custom banner message for this flag when NOT flagged',
              'type' => 'varchar',
              'length' => '256',
              'not null' => FALSE,
              'default' => '',
            ),
            self::FIELD_BANNER_COLOR_BG_FLAGGED => array(
              'description' => 'Color of the banner background when flagged.',
              'type' => 'varchar',
              'length' => '6',
              'not null' => TRUE,
              'default' => BannerFlagHelper::CSS_COLOR_DEFAULT_BG_FLAGGED,
            ),
            self::FIELD_BANNER_COLOR_BG_FLAGGED_NOT => array(
              'description' => 'Color of the banner background when NOT flagged.',
              'type' => 'varchar',
              'length' => '6',
              'not null' => TRUE,
              'default' => BannerFlagHelper::CSS_COLOR_DEFAULT_BG_FLAGGED_NOT,
            ),
            self::FIELD_BANNER_COLOR_TEXT_FLAGGED => array(
              'description' => 'Color of the banner text when flagged.',
              'type' => 'varchar',
              'length' => '6',
              'not null' => TRUE,
              'default' => BannerFlagHelper::CSS_COLOR_DEFAULT_TEXT_FLAGGED,
            ),
            self::FIELD_BANNER_COLOR_TEXT_FLAGGED_NOT => array(
              'description' => 'Color of the banner text when NOT flagged.',
              'type' => 'varchar',
              'length' => '6',
              'not null' => TRUE,
              'default' => BannerFlagHelper::CSS_COLOR_DEFAULT_TEXT_FLAGGED_NOT,
            ),
            self::FIELD_BANNER_COLOR_BORDER_FLAGGED => array(
              'description' => 'Color of the banner border when flagged.',
              'type' => 'varchar',
              'length' => '6',
              'not null' => TRUE,
              'default' => BannerFlagHelper::CSS_COLOR_DEFAULT_BORDER_FLAGGED,
            ),
            self::FIELD_BANNER_COLOR_BORDER_FLAGGED_NOT => array(
              'description' => 'Color of the banner border when NOT flagged.',
              'type' => 'varchar',
              'length' => '6',
              'not null' => TRUE,
              'default' => BannerFlagHelper::CSS_COLOR_DEFAULT_BORDER_FLAGGED_NOT,
            ),
          ),
          'primary key' => array(self::FIELD_FID),
        );
    }

    /**
     * Synchronise the Flag Plus tracking table with the main Flag table.
     *
     * If one or more Flag items have been added since the last sync
     * matching rows will be added to the Flag Plus banner table.
     *
     * If one or more Flag items have been deleted since the last sync
     * any unmatched rows will be deleted from the Flag Plus banner table.
     */
    static public function dbSyncTables() {

        $flags = flag_get_flags();

        // Add matching banner row for any new flags.
        foreach ($flags as $name => $flag) {
            $fid = $flag->fid;

            $query_exists_fid = 'SELECT ' . self::FIELD_FID . ' FROM {' . self::TABLE . '} WHERE ' .
                self::FIELD_FID . " = :" . self::FIELD_FID;
            $params = array(':' . self::FIELD_FID => $fid);

            $result = db_query($query_exists_fid, $params);

            $count = 0;
            foreach ($result as $record) {
                $count++;
            }

            if ($count < 1) {
                // Should only ever happen once, and only if not already in table.
                $record = array(self::FIELD_FID => $fid);
                drupal_write_record(self::TABLE, $record);
            }
        }

        // Remove un-matched banner rows from any deleted flags.
        $query_banner_fids = 'SELECT ' . self::FIELD_FID . ' FROM {' . self::TABLE . '}';
        $result_banner_fids = db_query($query_banner_fids);
        foreach ($result_banner_fids as $banner) {
            $bfid = $banner->fid;
            $match = FALSE;
            foreach ($flags as $name => $flag) {
                $fid = $flag->fid;
                $match = $match || $fid === $bfid;
            }
            if (!$match) {
                db_delete(self::TABLE)->condition(self::FIELD_FID, $bfid);
            }
        }
    }

}
