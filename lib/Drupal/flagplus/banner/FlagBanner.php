<?php

/**
 * @file
 * FlagBanner.
 */

namespace Drupal\flagplus\banner;

use Drupal\flagplus\Common;
use Drupal\flagplus\banner\BannerSchema;
use flag_flag;

// @codingStandardsIgnoreStart
// use DatabaseStatementInterface;
// For some reason fails as type hint, so using stdClass.
// @codingStandardsIgnoreEnd

use stdClass;
use Drupal\flagplus\banner\IFlagBanner;
use Drupal\flagplus\banner\IFlagBannerStateData;
use Drupal\flagplus\banner\FlagBannerStateData;
use Drupal\flagplus\banner\BannerHelper;
use Drupal\flagplus\banner\BannerFlagHelper;

/**
 * Default rectangular strip implementation of a banner specific to a Flag.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2515 FlagBanner @endlink.
 */
class FlagBanner implements IFlagBanner {

  /**
   * Factory method for a new Flag Plus banner state data manager product.
   *
   * This implementation returns a default banner state data manager
   * suitable for use with a rectangular banner strip, however it
   * could be used for any desired banner geometry.
   *
   * @param bool $show
   *   Whether to show the display message at all when in this state.
   * @param string $message
   *   The human-friendly flag message/description.
   * @param string $colorText
   *   A 6-character HEX color string for the banner text color.
   * @param string $colorBackground
   *   A 6-character HEX color string for the banner background color.
   * @param string $colorBorder
   *   A 6-character HEX color string for the banner border color.
   *
   * @return IFlagBannerStateData
   *   A new Flag Plus banner state data manager product.
   */
  static protected function myFlagBannerStateData($show, $message, $colorText, $colorBackground, $colorBorder) {
    return new FlagBannerStateData($show, $message, $colorText, $colorBackground, $colorBorder);
  }

  /**
   * A Flag handler.
   *
   * @var \flag_flag
   */
  private $flag;

  /**
   * The flag for which this manages a banner.
   *
   * @return \flag_flag
   *   The flag for which this manages a banner.
   */
  public function getFlag() {
    return $this->flag;
  }

  /**
   * The title of the flag.
   *
   * @return string
   *   The title of the flag.
   */
  protected function getFlagTitle() {
    return $this->flag->get_title();
  }

  /**
   * The name of the flag.
   *
   * @return string
   *   The name of the flag.
   */
  protected function getFlagName() {
    return $this->flag->name;
  }

  // @codingStandardsIgnoreStart
  // @param DatabaseStatementInterface $record
  // Issue: DatabaseStatementInterface Fails as type hint.
  // PHP ERROR: must implement interface DatabaseStatementInterface, instance of stdClass given.
  // Coder complains if use \stdClass (but it runs).
  // PHP complains (it fails) if use \object.
  // @codingStandardsIgnoreEnd

  /**
   * Constructs a Flag Plus banner manager for a given flag.
   *
   * @param \flag_flag $flag
   *   A flag handler.
   * @param \stdClass $record
   *   (Optional.) A record from a db query compatible with the FlagPlus table.
   */
  public function __construct(flag_flag $flag, \stdClass $record = NULL) {
    // @todo checks.
    $this->flag = $flag;
    if (!empty($record)) {
      $this->setFromDbRecord($record);
    }
  }

  /**
   * Placeholder message, just for init.
   *
   * The message should be eventually set from a DB value.
   *
   * @var string
   */
  const MESSAGE_DUMMY = '[message]';

  /**
   * The state data for this when flagged.
   *
   * @var IFlagBannerStateData
   */
  private $dataFlagged;

  /**
   * The state data for this when flagged.
   *
   * Lazily created.
   *
   * @return IFlagBannerStateData
   *   The state data for this when flagged.
   */
  public function getDataFlagged() {
    if (empty($this->dataFlagged)) {
      $show = TRUE;
      $this->dataFlagged = self::myFlagBannerStateData(
              $show, self::MESSAGE_DUMMY, BannerFlagHelper::CSS_COLOR_DEFAULT_TEXT_FLAGGED, BannerFlagHelper::CSS_COLOR_DEFAULT_BG_FLAGGED, BannerFlagHelper::CSS_COLOR_DEFAULT_BORDER_FLAGGED
      );
    }
    return $this->dataFlagged;
  }

  /**
   * The state data for this when not flagged.
   *
   * @var IFlagBannerStateData
   */
  private $dataNotFlagged;

  /**
   * The state data for this when flagged.
   *
   * Lazily created.
   *
   * @return IFlagBannerStateData
   *   The state data for this when not flagged.
   */
  public function getDataNotFlagged() {
    if (empty($this->dataNotFlagged)) {
      $show = TRUE;
      $this->dataNotFlagged = self::myFlagBannerStateData(
              $show, self::MESSAGE_DUMMY, BannerFlagHelper::CSS_COLOR_DEFAULT_TEXT_FLAGGED_NOT, BannerFlagHelper::CSS_COLOR_DEFAULT_BG_FLAGGED_NOT, BannerFlagHelper::CSS_COLOR_DEFAULT_BORDER_FLAGGED_NOT
      );
    }
    return $this->dataNotFlagged;
  }

  /**
   * Whether to show the flag name as a title/label.
   *
   * @var bool
   */
  private $showTitle;

  /**
   * Whether to show the flag name as a title/label.
   *
   * @return bool
   *   Whether to show the flag name as a title/label.
   */
  public function isShowTitle() {
    return $this->showTitle;
  }

  /**
   * Sets whether to show the flag name as a title/label.
   *
   * @param bool $showTitle
   *   Whether to show the flag name as a title/label.
   *
   * @return IFlagBanner
   *   This.
   */
  public function setShowTitle($showTitle) {
    $this->showTitle = $showTitle;
    return $this;
  }

  /**
   * Whether to show the a message for the flag.
   *
   * @var bool
   */
  private $showMessage;

  /**
   * Whether to show the a message for the flag.
   *
   * @return bool
   *   Whether to show the a message for the flag.
   */
  public function isShowMessage() {
    return $this->showMessage;
  }

  /**
   * Sets whether to show the a message for the flag.
   *
   * @param bool $showMessage
   *   Whether to show the a message for the flag.
   *
   * @return IFlagBanner
   *   This.
   */
  public function setShowMessage($showMessage) {
    $this->showMessage = $showMessage;
    return $this;
  }

  /**
   * Whether the given node (by id) is flagged by the flag of this.
   *
   * @param int $nid
   *   Node id.
   *
   * @return bool
   *   Whether the given node (by id) is flagged by the flag of this.
   */
  protected function getIsNodeFlagged($nid) {
    return $this->flag->is_flagged($nid);
  }

  /**
   * Builds a styled Flag Plus banner for the flag of this for a given node.
   *
   * @param bool $flagged
   *   Wether this is for flagged content.
   *
   * @return array
   *   A #markup render array portion for the banner.
   */
  public function buildBanner($flagged) {
    $flagname = $this->getFlagName();

    $class_base = Common::MODULE . '-banner';
    $class_label = Common::MODULE . '-banner-label';

    $class = $this->addCssBannerClass($flagged);
    $label = $this->buildLabel($flagged);

    $message = $flagged ? $this->getDataFlagged()->getMessage() : $this->getDataNotFlagged()->getMessage();

    $banner = $this->isShowMessage() ? check_plain($message) : '';
    return array(
      '#markup' => '<span class="' . $class_label . '"> ' . $label . '</span> ' . $banner,
      '#prefix' => '<div class="' . "$class_base $class $class-$flagname" . '">',
      '#suffix' => '</div>',
    );
  }

  /**
   * Adds inline CSS to style a banner depending on whether flagged.
   *
   * @param bool $flagged
   *   Whether this should be for a flagged node.
   *
   * @return string
   *   A string for a CSS class indicating whether an item is flagged or not.
   */
  protected function addCssBannerClass($flagged) {

    $flagname = $this->getFlagName();
    $class = $flagged ? Common::MODULE . '-banner-flagged' : Common::MODULE . '-banner-flagged-not';
    $class_flag = "$class-$flagname";
    $center = BannerHelper::isCssStyleCenterBanners() ? 'text-align: center;' : '';
    $color_text = $flagged ? $this->getDataFlagged()->getColorText() : $this->getDataNotFlagged()->getColorText();
    $color_bg = $flagged ? $this->getDataFlagged()->getColorBackground() : $this->getDataNotFlagged()->getColorBackground();
    $color_border = $flagged ? $this->getDataFlagged()->getColorBorder() : $this->getDataNotFlagged()->getColorBorder();
    $style = "color: #$color_text; background-color: #$color_bg; border-color: #$color_border; $center";
    $css_inline = "div.$class_flag { $style }";
    drupal_add_css($css_inline, array('type' => 'inline'));
    return $class;
  }

  /**
   * Creates a label string for a banner indicating whether flagged or not.
   *
   * If a message is also to be shown, the label will have a ':' colon appended.
   * Note the message is NOT yet included in the string returned by this,
   * but this function has to know whether a message will be used eventually.
   *
   * @param bool $flagged
   *   Whether this should be for a flagged node.
   *
   * @return string
   *   A string for the label (possibly empty) indicating the flag state.
   */
  protected function buildLabel($flagged) {
    $flagtitle = $this->getFlagTitle();
    $not = $flagged ? '' : 'NOT ';
    $colon = ($this->isShowMessage() && $this->isShowTitle()) ? ':' : '';
    $title = BannerHelper::isCssStyleCapsTitle() ? strtoupper($flagtitle) : $flagtitle;
    return $this->isShowTitle() ? $not . $title . $colon : '';
  }

  /**
   * Sets all of the per-flag banner options from a single DB row record.
   *
   * A stdClass is used as a type hint for the $record parameter
   * (a row from a DB query result) for building an IFlagBanner because
   * for some reason DatabaseStatementInterface is not accepted as a type hint.
   * Using object works, but does not pass Coder 8.x-2.x.
   * Consider instead encapsulating the query result set and
   * its row records in IBannerQueryResult and IBannerQueryRecord.
   *
   * @param stdClass $record
   *   A DB row record compatible with the Flag Plus banner table.
   *
   * @return IFlagBanner
   *   This.
   */
  public function setFromDbRecord(stdClass $record) {

    $this->setShowTitle($record->{BannerSchema::FIELD_BANNER_FLAG_TITLE_SHOW});
    $this->setShowMessage($record->{BannerSchema::FIELD_BANNER_MESSAGE_SHOW});

    $this->getDataFlagged()->setShow($record->{BannerSchema::FIELD_BANNER_ON_FLAGGED});
    $this->getDataNotFlagged()->setShow($record->{BannerSchema::FIELD_BANNER_ON_FLAGGED_NOT});

    $this->getDataFlagged()->setMessage($record->{BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED});
    $this->getDataNotFlagged()->setMessage($record->{BannerSchema::FIELD_BANNER_MESSAGE_FLAGGED_NOT});

    $this->getDataFlagged()->setColorText($record->{BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED});
    $this->getDataNotFlagged()->setColorText($record->{BannerSchema::FIELD_BANNER_COLOR_TEXT_FLAGGED_NOT});

    $this->getDataFlagged()->setColorBackground($record->{BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED});
    $this->getDataNotFlagged()->setColorBackground($record->{BannerSchema::FIELD_BANNER_COLOR_BG_FLAGGED_NOT});

    $this->getDataFlagged()->setColorBorder($record->{BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED});
    $this->getDataNotFlagged()->setColorBorder($record->{BannerSchema::FIELD_BANNER_COLOR_BORDER_FLAGGED_NOT});

    return $this;
  }

}
