<?php

/**
 * @file
 * Helper class for banners.
 */

namespace Drupal\flagplus\banner;

/**
 * Manages data specific to a Flag banner state.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2520 IFlagBannerStateData @endlink.
 */
interface IFlagBannerStateData {

  /**
   * Whether to show/display a banner when in this state.
   *
   * @return bool
   *   Whether to show/display a banner when in this state.
   */
  public function isShow();

  /**
   * Sets whether to show/display a banner when in this state.
   *
   * @param bool $show
   *   Whether to show/display a banner when in this state.
   *
   * @return FlagBannerStateData
   *   This.
   */
  public function setShow($show = TRUE);

  /**
   * The message to show.
   *
   * @return string
   *   The message to show.
   */
  public function getMessage();

  /**
   * Sets the message to show.
   *
   * @param string $message
   *   The message to show.
   *
   * @return IFlagBanner
   *   This.
   */
  public function setMessage($message);

  /**
   * A 6-character HEX color string for the banner text color.
   *
   * @return string
   *   A 6-character HEX color string for the banner text color.
   */
  public function getColorText();

  /**
   * Sets a 6-character HEX color string for the banner text color.
   *
   * @param string $colorText
   *   A 6-character HEX color string for the banner text color.
   *
   * @return IFlagBanner
   *   this.
   */
  public function setColorText($colorText);

  /**
   * A 6-character HEX color string for the banner bg color.
   *
   * @return string
   *   A 6-character HEX color string for the banner bg color.
   */
  public function getColorBackground();

  /**
   * Sets a 6-character HEX color string for the banner background color.
   *
   * @param string $colorBackground
   *   A 6-character HEX color string for the banner background color.
   *
   * @return IFlagBanner
   *   this.
   */
  public function setColorBackground($colorBackground);

  /**
   * A 6-character HEX color string for the banner border color.
   *
   * @return string
   *   A 6-character HEX color string for the banner border color.
   */
  public function getColorBorder();

  /**
   * Sets a 6-character HEX color string for the banner border color.
   *
   * @param string $colorBorder
   *   A 6-character HEX color string for the banner border color.
   *
   * @return IFlagBanner
   *   this.
   */
  public function setColorBorder($colorBorder);

}
