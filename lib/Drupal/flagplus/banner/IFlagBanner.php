<?php

/**
 * @file
 * IFlagBanner.
 */

namespace Drupal\flagplus\banner;

use Drupal\flagplus\banner\IFlagBannerStateData;

use flag_flag;

use stdClass;

/**
 * Manages a banner specific to a Flag.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2517 IFlagBanner @endlink.
 */
interface IFlagBanner {

  /**
   * The flag for which this manages a banner.
   *
   * @return \flag_flag
   *   The flag for which this manages a banner.
   */
  public function getFlag();

  /**
   * The state data for this when flagged.
   *
   * Lazily created.
   *
   * @return IFlagBannerStateData
   *   The state data for this when flagged.
   */
  public function getDataFlagged();

  /**
   * The state data for this when flagged.
   *
   * Lazily created.
   *
   * @return IFlagBannerStateData
   *   The state data for this when not flagged.
   */
  public function getDataNotFlagged();

  /**
   * Whether to show the flag name as a title/label.
   *
   * @return bool
   *   Whether to show the flag name as a title/label.
   */
  public function isShowTitle();

  /**
   * Sets whether to show the flag name as a title/label.
   *
   * @param bool $showTitle
   *   Whether to show the flag name as a title/label.
   *
   * @return IFlagBanner
   *   This.
   */
  public function setShowTitle($showTitle);

  /**
   * Whether to show the a message (in addition to a label) for the flag.
   *
   * @return bool
   *   Whether to show a message (in addition to a label) for the flag.
   */
  public function isShowMessage();

  /**
   * Sets whether to show a message (in addition to a label) for the flag.
   *
   * @param bool $showMessage
   *   Whether to show a message (in addition to a label) for the flag.
   *
   * @return IFlagBanner
   *   This.
   */
  public function setShowMessage($showMessage);

  /**
   * Builds a styled Flag Plus banner for the flag of this for a given state.
   *
   * @param bool $flagged
   *   Wether this is for flagged content.
   *
   * @return array
   *   A #markup render array portion for the banner.
   */
  public function buildBanner($flagged);

  /**
   * Sets all of the per-flag banner options from a single DB row record.
   *
   * @param stdClass $record
   *   A DB row record compatible with the Flag Plus banner table.
   *
   * @return IFlagBanner
   *   This.
   */
  public function setFromDbRecord(stdClass $record);

}
