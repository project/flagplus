<?php

/**
 * @file
 * FlagBannerStateData.
 */

namespace Drupal\flagplus\banner;

use Drupal\flagplus\banner\IFlagBannerStateData;

/**
 * Implements management of data specific to a Flag banner state.
 *
 * This version is suitable for use for a simple rectangular
 * strip with a text color, background color, and border color.
 * The same data could however be used for other banner designs.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2516 FlagBannerStateData @endlink.
 */
class FlagBannerStateData implements IFlagBannerStateData {

  /**
   * Constructs a Flag Plus banner state data manager.
   *
   * @param bool $show
   *   Whether to show the display message at all when in this state.
   * @param string $message
   *   The human-friendly flag message/description.
   * @param string $colorText
   *   A 6-character HEX color string for the banner text color.
   * @param string $colorBackground
   *   A 6-character HEX color string for the banner background color.
   * @param string $colorBorder
   *   A 6-character HEX color string for the banner border color.
   */
  public function __construct($show, $message, $colorText, $colorBackground, $colorBorder) {
    $this->show = $show;
    $this->message = $message;
    // @todo color string format checks.
    $this->colorText = $colorText;
    $this->colorBackground = $colorBackground;
    $this->colorBorder = $colorBorder;
  }

  /**
   * Whether to show/display a banner when in this state.
   *
   * @var bool
   */
  private $show = TRUE;

  /**
   * Whether to show/display a banner when in this state.
   *
   * @return bool
   *   Whether to show/display a banner when in this state.
   */
  public function isShow() {
    return $this->show;
  }

  /**
   * Sets whether to show/display a banner when in this state.
   *
   * @param bool $show
   *   Whether to show/display a banner when in this state.
   *
   * @return FlagBannerStateData
   *   This.
   */
  public function setShow($show = TRUE) {
    $this->show = $show;
    return $this;
  }

  /**
   * The message to show.
   *
   * @var string
   */
  private $message;

  /**
   * The message to show.
   *
   * @return string
   *   The message to show.
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * Sets the message to show.
   *
   * @param string $message
   *   The message to show.
   *
   * @return IFlagBanner
   *   This.
   */
  public function setMessage($message) {
    $this->message = $message;
    return $this;
  }


  /**
   * A 6-character HEX color string for the banner text color.
   *
   * @var string
   */
  private $colorText;

  /**
   * A 6-character HEX color string for the banner text color.
   *
   * @return string
   *   A 6-character HEX color string for the banner text color.
   */
  public function getColorText() {
    return $this->colorText;
  }

  /**
   * Sets a 6-character HEX color string for the banner text color.
   *
   * @param string $colorText
   *   A 6-character HEX color string for the banner text color.
   *
   * @return IFlagBanner
   *   this.
   */
  public function setColorText($colorText) {
    $this->colorText = $colorText;
    return $this;
  }

  /**
   * A 6-character HEX color string for the banner background color.
   *
   * @var string
   */
  private $colorBackground;

  /**
   * A 6-character HEX color string for the banner bg color.
   *
   * @return string
   *   A 6-character HEX color string for the banner bg color.
   */
  public function getColorBackground() {
    return $this->colorBackground;
  }

  /**
   * Sets a 6-character HEX color string for the banner background color.
   *
   * @param string $colorBackground
   *   A 6-character HEX color string for the banner background color.
   *
   * @return IFlagBanner
   *   this.
   */
  public function setColorBackground($colorBackground) {
    $this->colorBackground = $colorBackground;
    return $this;
  }

  /**
   * A 6-character HEX color string for the banner border color.
   *
   * @var string
   */
  private $colorBorder;

  /**
   * A 6-character HEX color string for the banner border color.
   *
   * @return string
   *   A 6-character HEX color string for the banner border color.
   */
  public function getColorBorder() {
    return $this->colorBorder;
  }

  /**
   * Sets a 6-character HEX color string for the banner border color.
   *
   * @param string $colorBorder
   *   A 6-character HEX color string for the banner border color.
   *
   * @return IFlagBanner
   *   this.
   */
  public function setColorBorder($colorBorder) {
    $this->colorBorder = $colorBorder;
    return $this;
  }

}
