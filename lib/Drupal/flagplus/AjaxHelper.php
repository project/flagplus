<?php

/**
 * @file
 * AjaxHelper.
 */

namespace Drupal\flagplus;

use Drupal\flagplus\Common;

/**
 * Helper class for AJAX.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2475 AjaxHelper @endlink.
 */
class AjaxHelper {

  const STEM_AJAX_CSS = 'css/flagplus_ajax.css';
  const STEM_AJAX_JS = 'js/flagplus_ajax.js';

  /**
   * For hiding/showing element depending on JavaScript support.
   *
   * @param array $form
   *   Form API form array.
   */
  static public function attachAjaxHideShow(array &$form) {

    $form['#attached']['css'] = array(
      drupal_get_path('module', Common::MODULE) . '/' . self::STEM_AJAX_CSS,
    );
    $form['#attached']['js'] = array(
      drupal_get_path('module', Common::MODULE) . '/' . self::STEM_AJAX_JS,
    );
  }

  /**
   * Tries to reload a page using AJAX to show banner changes on flag/unflag.
   *
   * BUGGY: WIP: Issue: support request:
   * @link https://www.drupal.org/node/2403131 @endlink.
   *
   * @param int $content_id
   *   The identifier of the content, usually a node id.
   */
  static public function reloadPage($content_id) {
    // Load Drupal core AJAX library.
    drupal_add_library('system', 'drupal.ajax');
    drupal_add_js('misc/jquery.form.js');

    // Check whether exists as a node and theme it.
    $node = node_load($content_id);
    if ($node) {

      $vnode = node_view($node);
      $vnode_themed = theme('node', $vnode);

      // An AJAX command that replaces the page #content.
      // @codingStandardsIgnoreStart
      $selector = '#content'; // FAILS
      //$selector = 'div.content'; //FAILS
      // @codingStandardsIgnoreEnd
      $html_replace = $vnode_themed;
      // Optional: $settings.
      $commands[] = ajax_command_replace($selector, $html_replace);
      $page = array(
        '#type' => 'ajax',
        '#commands' => $commands,
      );
      ajax_deliver($page);
    }
  }

  // @codingStandardsIgnoreStart
  /**
   * BUGGY: WIP: Issue: support request: https://www.drupal.org/node/2403131
   *
   * @param type $content_id
   */
  static public function reloadPageCtoolsFails($content_id) {

    ctools_include('ajax');
    ctools_ajax_command_reload(); //FAILS
    // Just to check whether exists as node
    $node = node_load($content_id);

    if ($node) {
      ctools_ajax_command_redirect("node/$content_id"); //FAILS
      //ctools_ajax_command_redirect("/node/$content_id");//FAILS
      //drupal_goto("/node/$content_id"); //FAILS
    }
  }
  //@codingStandardsIgnoreEnd

}
