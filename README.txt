This README.txt is adapted from the Drupal.org README Template:

 * https://www.drupal.org/node/2181737

For detailed version Flag Plus requirements, configuration instructions
and caveats please visit: https://www.drupal.org/project/flagplus

INTRODUCTION
------------

The simple-yet-powerful Flag project (https://www.drupal.org/project/flag) 
is one of the most popular Drupal contributed module projects. 

The Flag Plus project extends Flag with some convenient features such as:

* The ability to view and set flag applicability by entity bundle type 
  (so that one does not have to edit every flag and select it as applicable 
  to a newly added custom type, which can be tedious and error prone).

  CAVEAT: the developer has so far only used this capability for node flags.

  DISCLAIMER: this writes Flag database table data (via the Flag API);
  it is used regularly on the developer's own sites, but testing is minimal,
  use at your own risk.

* An easy system for indicating whether a given content node/page is flagged or
  NOT flagged with an easily-configured rectangular flag banner and CSS style options
  and classes. The message shown in the banner may be customised for each flag,
  as can the banner CSS text color, background color, and banner border color.

  Note that Flag alone has subtle indicators for when content is flagged but not
  for when content is NOT flagged. Indicating when something is NOT flagged is
  very useful if one uses for example a 'resolved' flag for say Issue content.

  One can choose for each flag whether a banner is shown or not for flagged or
  NOT flagged content. For example, for Issue content one might wish to show
  when it is NOT 'resolved', but for Article or Page content one might not wish
  to show a banner when it is NOT flagged as 'demo' content, only when it is.

* Future releases will contain additional features to complement Flag.

This is an actively maintained and evolving project,
with a live demonstration site project home page:

* http://drupal7demo.webel.com.au/module/flagplus


REQUIREMENTS
------------

This module requires:

- Flag module: https://www.drupal.org/project/flag
  (at least version 7.x-3.0).

- X Autoload: http://drupal.org/project/xautoload
  (at least version xautoload-7.x-5.2)

- Libraries module: http://drupal.org/project/libraries
  (at least version 7.x-2.1)

- JQuery Colorpicker module: https://www.drupal.org/project/jquery_colorpicker
  (at least version 7.x-1.1)

- JQuery Colorpicker library: http://www.eyecon.ro/colorpicker/#download


INSTALLATION
------------

 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information. Consider using the Drush command line tool.

 * Remember to download and install the Colorpick library by following
   these instructions https://www.drupal.org/node/1331988.


CONFIGURATION
-------------
 
 * There is a configuration overview page with instructions at:

   /admin/structure/flagplus

 * You must have at least flag administration rights to peforrm
   Flag Plus power editing operations such as flag applicability.

 * FLAG APPLICABILITY BY BUNDLE
   You can configure which flags are applicable to which bundles "by bundle".
   This is convenient when new entity type bundles (custom types) are added:

   /admin/structure/flagplus/bybundle/edit

   /admin/structure/flagplus/bybundle/ppu (AJAX version)
 
 * FLAG PLUS BANNERS

   A rectangular strip Flag Plus banner can be shown for any existing Flag.

   The banner can be shown via a page decorator (preferred) or via
   a banner block (more flexible w.r.t, placement in a given theme).
    
   There is an option for whether the Flag Plus banner is shown for node
   content, as well as an option to show a title and highlight the banner.
   Configure them at:

   /admin/config/structure/flagplus/banners

   You must have at least Flag Plus banner administration rights
   to edit Flag Plus banner styles and policies.

   Remember to also allocate the Flag Plus banner block (if used) to a theme
   region (usually to the first/top block within the Content region) at:

   /admin/structure/blocks

   You must have at least Flag Plus banner view rights to view 
   the Flag Plus banner strips in content pages and blocks
   (as well as permission to the view the block in the theme).

   Because the Flag module does not yet use Drupal7=-style AJAX a Flag Plus banner 
   will not visually update without a page reload (does not use AJAX refresh)
   if you change a Flag state using an AJAX Flag link, so please on creating or editing
   a Flag choose the option to use a non-AJAX Normal refresh link NOT a JavaScript toggle.

ISSUE REPORTING
---------------

Drupal.org (sandbox) project page:
  https://drupal.org/sandbox/webel/2398617

To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/2398617


MAINTAINERS
-----------

webel - http://drupal.org/user/392755

No other maintainers are currently sought.


CREDITS
-------

The Flag Plus project/module template is developed by Webel IT Australia,
specialists in PHP-driven Drupal CMS web engineering, UML, SysML, Java and XML.
In addition to using Drupal CMS for developing clients' web sites, Webel has
used Drupal CMS for many years to develop educational web sites promoting 
graphical software engineering with Unified Modeling Language (UML) and 
graphical systems engineering with Systems Modeling Language (SysML).

  http://www.webel.com.au

  http://drupal7demo.webel.com.au


SPONSORS SOUGHT
---------------

If you are interesting in seeing Flag Plus expanded, or in seeing a Drupal8
version developed, please consider supporting the project. Donations to the
project may be made online via PayPal at:

  http://drupal7demo.webel.com.au/lm_paypal/donations